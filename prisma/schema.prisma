// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

// Looking for ways to speed up your queries, or scale easily with your serverless or edge functions?
// Try Prisma Accelerate: https://pris.ly/cli/accelerate-init

generator client {
  provider        = "prisma-client-js"
  previewFeatures = ["postgresqlExtensions"]
}

datasource db {
  provider   = "postgresql"
  url        = env("DATABASE_URL")
  extensions = [unaccent, pg_trgm, fuzzystrmatch]
}

enum UserRoleEnum {
  ADMIN
  USER
  GUEST
}

enum TranslationModeEnum {
  TEXT
  HTML
}

model Atlas {
  id Int @id

  name        String @db.VarChar(64) // numéro de la série en texte
  nameNoSpace String @unique @db.VarChar(64) // numéro de la série en texte sans espace

  sourceId Int
  source   Source @relation(fields: [sourceId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  sourceNb Int

  nb Int
}

model AuthFenceLog {
  id        String   @id() @default(uuid()) @db.Uuid
  ip        String   @db.VarChar(64)
  createdAt DateTime @default(now())

  login String @db.VarChar(64)

  @@index([ip, createdAt])
}

model AuthToken {
  id String @id() @default(uuid()) @db.Uuid

  userId String @db.Uuid
  user   User   @relation(fields: [userId], references: [id])

  createdAt DateTime  @default(now())
  expiresAt DateTime?

  userAgent String @db.VarChar(256)
}

model User {
  id String @id() @default(uuid()) @db.Uuid

  provider   String?
  providerId String?

  name  String @unique @db.VarChar(64)
  email String @unique @db.VarChar(64)

  emailVerifyToken String? @db.VarChar(256)
  emailVerified    String? @db.VarChar(256) // store last verified email address

  password            String? @db.VarChar(256)
  forgotPasswordToken String? @db.VarChar(256)

  firstName String? @db.VarChar(256)
  lastName  String? @db.VarChar(256)

  isSuperAdmin Boolean      @default(false)
  role         UserRoleEnum @default(GUEST)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  activatedAt DateTime?
  expiresAt   DateTime?

  authTokens AuthToken[]

  langId String? @default("fr") @db.Char(2)
  lang   Lang?   @relation(fields: [langId], references: [id])

  bookmarks Bookmark[]
  // bookmarkLabels BookmarkLabel[]

  @@unique([provider, providerId])
}

model Bookmark {
  id Int @id() @default(autoincrement())

  userId String @db.Uuid
  user   User   @relation(fields: [userId], references: [id], onDelete: Cascade, onUpdate: Cascade)

  label String @default("default")

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  bookmarkToSers BookmarkToSer[]

  @@unique([userId, label])
}

model BookmarkToSer {
  bookmarkId Int
  serId      Int

  bookmark Bookmark @relation(fields: [bookmarkId], references: [id], onDelete: Cascade, onUpdate: Cascade)
  ser      Ser      @relation(fields: [serId], references: [id], onDelete: Cascade, onUpdate: Cascade)

  createdAt DateTime @default(now())

  @@id([bookmarkId, serId])
}

// model Bookmark {
//   id Int @id() @default(autoincrement())

//   userId String @db.Uuid
//   user   User   @relation(fields: [userId], references: [id], onDelete: Cascade, onUpdate: Cascade)

//   serId Int
//   ser   Ser @relation(fields: [serId], references: [id], onDelete: Restrict, onUpdate: Cascade)

//   createdAt DateTime @default(now())
// }

model Lang {
  id        String @id @db.Char(2) // ISO two letter language code
  name      String @unique @db.VarChar(64) // English name
  localName String @db.VarChar(64) // Local name of language

  categoryContents CategoryContent[]
  keywords         Keyword[]
  pictureContents  PictureContent[]
  serContents      SerContent[]
  sourceContents   SourceContent[]
  tagContents      TagContent[]

  users User[]
}

// On peut supprimer une série numérique (Ser) et tout ce qui y est lié sera supprimé en cascade
// On ne peut supprimer une Source si elle est liée à une série numérique (Ser)

// Séries Numériques
model Ser {
  id Int @id @default(autoincrement())

  name        String @db.VarChar(64) // numéro de la série en texte
  nameNoSpace String @unique @db.VarChar(64) // numéro de la série en texte sans espace

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  edges    SerEdge[] @relation("from")
  revEdges SerEdge[] @relation("to")

  sourceId        Int?
  sourceReference String?

  source Source? @relation(fields: [sourceId], references: [id], onUpdate: Cascade, onDelete: Restrict)

  categories      Category[]
  contents        SerContent[]
  keywords        Keyword[]
  pictureCaptions PictureCaptionToSer[]
  pictures        Picture[]
  // serToSources    SerToSource[]
  tags            Tag[]
  bookmarkToSers  BookmarkToSer[]

  order Int?

  @@index([order])
}

model SerContent {
  id Int @id @default(autoincrement())

  serId  Int
  langId String @db.Char(2)

  title   String @db.VarChar(512)
  content String @db.Text
  excerpt String @db.Text

  noAccentTitle String @db.VarChar(512)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  lang Lang @relation(fields: [langId], references: [id], onUpdate: Cascade, onDelete: Cascade)
  ser  Ser  @relation(fields: [serId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  @@unique([langId, serId])
  // @@unique([title, langId, serId]) // le titre devrait être unique pour une langue ?
  @@index([noAccentTitle])
}

model SerEdge {
  fromId Int
  toId   Int

  order Int?

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  from Ser @relation("from", fields: [fromId], references: [id], onUpdate: Cascade, onDelete: Cascade)
  to   Ser @relation("to", fields: [toId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  @@id([fromId, toId])
}

// model SerToSource {
//   serId    String @db.VarChar(64)
//   sourceId Int

//   reference   String?
//   description String?

//   createdAt DateTime @default(now())
//   updatedAt DateTime @updatedAt

//   ser    Ser    @relation(fields: [serId], references: [id], onUpdate: Cascade, onDelete: Cascade)
//   source Source @relation(fields: [sourceId], references: [id], onUpdate: Cascade, onDelete: Restrict)

//   @@id([serId, sourceId])
// }

model Picture {
  id Int @id @default(autoincrement())

  path String @unique @db.VarChar(512)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  captionSers     PictureCaptionToSer[]
  pictureContents PictureContent[]
  sers            Ser[]
}

model PictureContent {
  pictureId Int
  langId    String @db.VarChar(2)

  title       String  @db.VarChar(512)
  description String?

  lang    Lang    @relation(fields: [langId], references: [id], onUpdate: Cascade, onDelete: Cascade)
  picture Picture @relation(fields: [pictureId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  @@id([pictureId, langId])
}

model PictureCaptionToSer {
  pictureId Int
  serId     Int

  order  String? @db.VarChar(64) // le numéro ou lettre en légende.
  orderN Int?
  orderA String? @db.VarChar(64)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  picture Picture @relation(fields: [pictureId], references: [id], onUpdate: Cascade, onDelete: Restrict)
  ser     Ser     @relation(fields: [serId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  @@id([serId, pictureId])
  @@unique([pictureId, order])
  @@unique([pictureId, orderN, orderA])
}

model Source {
  id Int @id @default(autoincrement())

  tmpTitle String? @unique @db.VarChar(512)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  order Int?

  contents SourceContent[]
  // serToSources SerToSource[]
  sers     Ser[]

  atlases Atlas[] @relation()

  @@index([order])
}

model SourceContent {
  sourceId Int
  langId   String @db.Char(2)

  title       String  @db.VarChar(512)
  description String? @db.Text
  isbn        String? @db.VarChar(64)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  lang   Lang   @relation(fields: [langId], references: [id], onUpdate: Cascade, onDelete: Cascade)
  source Source @relation(fields: [sourceId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  @@id([langId, sourceId])
  @@unique([langId, title])
  @@unique([langId, isbn])
}

model Tag {
  id Int @id @default(autoincrement())

  tmpName String? @unique @db.VarChar(512)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  contents TagContent[]
  sers     Ser[]
}

model TagContent {
  tagId  Int
  langId String @db.Char(2)

  name        String  @db.VarChar(512)
  description String?

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  lang Lang @relation(fields: [langId], references: [id], onUpdate: Cascade, onDelete: Cascade)
  tag  Tag  @relation(fields: [tagId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  @@id([langId, tagId])
  @@unique([langId, tagId, name])
}

model Keyword {
  id Int @id @default(autoincrement())

  langId String @db.Char(2)

  word         String @db.VarChar(512)
  noAccentWord String @db.VarChar(512)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  lang Lang  @relation(fields: [langId], references: [id], onUpdate: Cascade, onDelete: Cascade)
  sers Ser[]

  // have a usable index to search keyword without lang? would have to be inverted
  @@unique([langId, noAccentWord])
}

model Category {
  id Int @id @default(autoincrement())

  parentId Int?
  order    Int?
  tmpName  String? @unique @db.VarChar(512)

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  children Category[]        @relation("parent")
  contents CategoryContent[]
  parent   Category?         @relation("parent", fields: [parentId], references: [id], onUpdate: Cascade, onDelete: Restrict)
  sers     Ser[]

  @@index([parentId, order])
}

model CategoryContent {
  categoryId Int
  langId     String @db.Char(2)

  name        String  @db.VarChar(512)
  description String?

  createdAt DateTime @default(now())
  updatedAt DateTime @updatedAt

  category Category @relation(fields: [categoryId], references: [id], onUpdate: Cascade, onDelete: Cascade)
  lang     Lang     @relation(fields: [langId], references: [id], onUpdate: Cascade, onDelete: Cascade)

  @@id([langId, categoryId])
  @@unique([name, langId])
}

model imp2 {
  id                Int     @id @default(autoincrement())
  ser_name          String?
  parent_ser_name   String?
  source            String?
  titre             String?
  categorie         String?
  description       String?
  mots_cles1        String?
  mots_cles2        String?
  ser_name_no_space String?

  @@index([ser_name_no_space])
}

model imp2atlas {
  id          Int    @id
  name        String @db.VarChar(64)
  source      String
  sourceNb    Int
  nb          Int
  nameNoSpace String @db.VarChar(64)

  @@index([nameNoSpace])
}

// model FailedImp {
//   id                Int     @id @default(autoincrement())
//   ser_name          String?
//   parent_ser_name   String?
//   source            String?
//   titre             String?
//   categorie         String?
//   description       String?
//   mots_cles1        String?
//   mots_cles2        String?
//   ser_name_no_space String?
// }

// model DuplicateImp {
//   id                Int     @id @default(autoincrement())
//   ser_name          String?
//   parent_ser_name   String?
//   source            String?
//   titre             String?
//   categorie         String?
//   description       String?
//   mots_cles1        String?
//   mots_cles2        String?
//   ser_name_no_space String?
// }
