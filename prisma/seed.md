# Import

```bash
echo ".import import.csv imp --csv" | sqlite3 import.sqlite
pgloader sqlite://import.sqlite pgsql://antony:antony@localhost/ggserie


db=ggserie sqlitedb=tmp/imp.sqlite csv=tmp/imp.csv \
  && ( echo "DROP DATABASE $db WITH (force); CREATE DATABASE $db;" | psql ) \
  && rm $sqlitedb && ( echo ".import $csv imp --csv" | sqlite3 $sqlitedb ) \
  && npx prisma migrate dev && pgloader sqlite://$sqlitedb pgsql://antony:antony@localhost/$db \
  && npx prisma db seed



# db=ggserie sqlitedb=tmp/imp.sqlite csv=tmp/imp.csv \
#   && rm $sqlitedb && ( echo ".import $csv imp --csv" | sqlite3 $sqlitedb ) \
#   && npx prisma migrate dev && pgloader sqlite://$sqlitedb pgsql://antony:antony@localhost/$db \
#   && npx prisma db seed


db=ggserie sqlitedb=tmp/import-atlas.sqlite csv=tmp/import-atlas.csv \
  && rm "$sqlitedb" \
  && ( echo ".import $csv impAtlas --csv" | sqlite3 $sqlitedb ) \
  && pgloader sqlite://$sqlitedb pgsql://antony:antony@localhost/$db

( echo "DROP DATABASE ggserie WITH (force); CREATE DATABASE ggserie; " | psql )   && rm -f tmp/imp.sqlite && ( echo ".import tmp/import.csv imp --csv" | sqlite3 tmp/imp.sqlite )   && npx prisma migrate dev && pgloader sqlite://tmp/imp.sqlite pgsql://antony:antony@localhost/ggserie && npx prisma db seed
```
