-- CreateTable
CREATE TABLE "Bookmark" (
    "id" SERIAL NOT NULL,
    "userId" UUID NOT NULL,
    "label" TEXT NOT NULL DEFAULT 'default',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Bookmark_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "BookmarkToSer" (
    "bookmarkId" INTEGER NOT NULL,
    "serId" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "BookmarkToSer_pkey" PRIMARY KEY ("bookmarkId","serId")
);

-- CreateIndex
CREATE UNIQUE INDEX "Bookmark_userId_label_key" ON "Bookmark"("userId", "label");

-- AddForeignKey
ALTER TABLE "Bookmark" ADD CONSTRAINT "Bookmark_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "BookmarkToSer" ADD CONSTRAINT "BookmarkToSer_bookmarkId_fkey" FOREIGN KEY ("bookmarkId") REFERENCES "Bookmark"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "BookmarkToSer" ADD CONSTRAINT "BookmarkToSer_serId_fkey" FOREIGN KEY ("serId") REFERENCES "Ser"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
