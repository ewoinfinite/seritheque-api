-- CreateTable
CREATE TABLE "imp2" (
    "id" SERIAL NOT NULL,
    "ser_name" TEXT,
    "parent_ser_name" TEXT,
    "source" TEXT,
    "titre" TEXT,
    "categorie" TEXT,
    "description" TEXT,
    "mots_cles1" TEXT,
    "mots_cles2" TEXT,
    "ser_name_no_space" TEXT,

    CONSTRAINT "imp2_pkey" PRIMARY KEY ("id")
);
