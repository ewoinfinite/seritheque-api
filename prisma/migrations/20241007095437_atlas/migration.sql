/*
  Warnings:

  - A unique constraint covering the columns `[nameNoSpace]` on the table `Ser` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "Ser_nameNoSpace_idx";

-- DropIndex
DROP INDEX "Ser_name_key";

-- AlterTable
ALTER TABLE "Ser" ADD COLUMN     "order" INTEGER;

-- AlterTable
ALTER TABLE "Source" ADD COLUMN     "order" INTEGER;

-- CreateTable
CREATE TABLE "Atlas" (
    "id" INTEGER NOT NULL,
    "name" VARCHAR(64) NOT NULL,
    "nameNoSpace" VARCHAR(64) NOT NULL,
    "sourceId" INTEGER NOT NULL,
    "sourceNb" INTEGER NOT NULL,
    "nb" INTEGER NOT NULL,

    CONSTRAINT "Atlas_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "imp2atlas" (
    "id" INTEGER NOT NULL,
    "name" VARCHAR(64) NOT NULL,
    "source" TEXT NOT NULL,
    "sourceNb" INTEGER NOT NULL,
    "nb" INTEGER NOT NULL,
    "nameNoSpace" VARCHAR(64) NOT NULL,

    CONSTRAINT "imp2atlas_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Atlas_nameNoSpace_key" ON "Atlas"("nameNoSpace");

-- CreateIndex
CREATE INDEX "imp2atlas_nameNoSpace_idx" ON "imp2atlas"("nameNoSpace");

-- CreateIndex
CREATE UNIQUE INDEX "Ser_nameNoSpace_key" ON "Ser"("nameNoSpace");

-- CreateIndex
CREATE INDEX "Ser_order_idx" ON "Ser"("order");

-- CreateIndex
CREATE INDEX "Source_order_idx" ON "Source"("order");

-- CreateIndex
CREATE INDEX "imp2_ser_name_no_space_idx" ON "imp2"("ser_name_no_space");

-- AddForeignKey
ALTER TABLE "Atlas" ADD CONSTRAINT "Atlas_sourceId_fkey" FOREIGN KEY ("sourceId") REFERENCES "Source"("id") ON DELETE CASCADE ON UPDATE CASCADE;
