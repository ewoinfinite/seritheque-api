/*
  Warnings:

  - The primary key for the `PictureCaptionToSer` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Ser` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The `id` column on the `Ser` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - The primary key for the `SerEdge` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `tmpName` on the `Source` table. All the data in the column will be lost.
  - You are about to drop the `SerToSource` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[langId,serId]` on the table `SerContent` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[tmpTitle]` on the table `Source` will be added. If there are existing duplicate values, this will fail.
  - Changed the type of `serId` on the `PictureCaptionToSer` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Added the required column `nameNoSpace` to the `Ser` table without a default value. This is not possible if the table is not empty.
  - Changed the type of `serId` on the `SerContent` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `fromId` on the `SerEdge` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `toId` on the `SerEdge` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `B` on the `_CategoryToSer` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `B` on the `_KeywordToSer` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `B` on the `_PictureToSer` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `A` on the `_SerToTag` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- DropForeignKey
ALTER TABLE "PictureCaptionToSer" DROP CONSTRAINT "PictureCaptionToSer_serId_fkey";

-- DropForeignKey
ALTER TABLE "SerContent" DROP CONSTRAINT "SerContent_serId_fkey";

-- DropForeignKey
ALTER TABLE "SerEdge" DROP CONSTRAINT "SerEdge_fromId_fkey";

-- DropForeignKey
ALTER TABLE "SerEdge" DROP CONSTRAINT "SerEdge_toId_fkey";

-- DropForeignKey
ALTER TABLE "SerToSource" DROP CONSTRAINT "SerToSource_serId_fkey";

-- DropForeignKey
ALTER TABLE "SerToSource" DROP CONSTRAINT "SerToSource_sourceId_fkey";

-- DropForeignKey
ALTER TABLE "_CategoryToSer" DROP CONSTRAINT "_CategoryToSer_B_fkey";

-- DropForeignKey
ALTER TABLE "_KeywordToSer" DROP CONSTRAINT "_KeywordToSer_B_fkey";

-- DropForeignKey
ALTER TABLE "_PictureToSer" DROP CONSTRAINT "_PictureToSer_B_fkey";

-- DropForeignKey
ALTER TABLE "_SerToTag" DROP CONSTRAINT "_SerToTag_A_fkey";

-- DropIndex
DROP INDEX "SerContent_title_langId_serId_key";

-- DropIndex
DROP INDEX "Source_tmpName_key";

-- AlterTable
ALTER TABLE "PictureCaptionToSer" DROP CONSTRAINT "PictureCaptionToSer_pkey",
DROP COLUMN "serId",
ADD COLUMN     "serId" INTEGER NOT NULL,
ADD CONSTRAINT "PictureCaptionToSer_pkey" PRIMARY KEY ("serId", "pictureId");

-- AlterTable
ALTER TABLE "Ser" DROP CONSTRAINT "Ser_pkey",
ADD COLUMN     "nameNoSpace" VARCHAR(64) NOT NULL,
ADD COLUMN     "sourceId" INTEGER,
ADD COLUMN     "sourceReference" TEXT,
DROP COLUMN "id",
ADD COLUMN     "id" SERIAL NOT NULL,
ADD CONSTRAINT "Ser_pkey" PRIMARY KEY ("id");

-- AlterTable
ALTER TABLE "SerContent" DROP COLUMN "serId",
ADD COLUMN     "serId" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "SerEdge" DROP CONSTRAINT "SerEdge_pkey",
DROP COLUMN "fromId",
ADD COLUMN     "fromId" INTEGER NOT NULL,
DROP COLUMN "toId",
ADD COLUMN     "toId" INTEGER NOT NULL,
ADD CONSTRAINT "SerEdge_pkey" PRIMARY KEY ("fromId", "toId");

-- AlterTable
ALTER TABLE "Source" DROP COLUMN "tmpName",
ADD COLUMN     "tmpTitle" VARCHAR(512);

-- AlterTable
ALTER TABLE "_CategoryToSer" DROP COLUMN "B",
ADD COLUMN     "B" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "_KeywordToSer" DROP COLUMN "B",
ADD COLUMN     "B" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "_PictureToSer" DROP COLUMN "B",
ADD COLUMN     "B" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "_SerToTag" DROP COLUMN "A",
ADD COLUMN     "A" INTEGER NOT NULL;

-- DropTable
DROP TABLE "SerToSource";

-- CreateIndex
CREATE INDEX "Ser_nameNoSpace_idx" ON "Ser"("nameNoSpace");

-- CreateIndex
CREATE UNIQUE INDEX "SerContent_langId_serId_key" ON "SerContent"("langId", "serId");

-- CreateIndex
CREATE UNIQUE INDEX "Source_tmpTitle_key" ON "Source"("tmpTitle");

-- CreateIndex
CREATE UNIQUE INDEX "_CategoryToSer_AB_unique" ON "_CategoryToSer"("A", "B");

-- CreateIndex
CREATE INDEX "_CategoryToSer_B_index" ON "_CategoryToSer"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_KeywordToSer_AB_unique" ON "_KeywordToSer"("A", "B");

-- CreateIndex
CREATE INDEX "_KeywordToSer_B_index" ON "_KeywordToSer"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_PictureToSer_AB_unique" ON "_PictureToSer"("A", "B");

-- CreateIndex
CREATE INDEX "_PictureToSer_B_index" ON "_PictureToSer"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_SerToTag_AB_unique" ON "_SerToTag"("A", "B");

-- AddForeignKey
ALTER TABLE "Ser" ADD CONSTRAINT "Ser_sourceId_fkey" FOREIGN KEY ("sourceId") REFERENCES "Source"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerContent" ADD CONSTRAINT "SerContent_serId_fkey" FOREIGN KEY ("serId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerEdge" ADD CONSTRAINT "SerEdge_fromId_fkey" FOREIGN KEY ("fromId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerEdge" ADD CONSTRAINT "SerEdge_toId_fkey" FOREIGN KEY ("toId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PictureCaptionToSer" ADD CONSTRAINT "PictureCaptionToSer_serId_fkey" FOREIGN KEY ("serId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SerToTag" ADD CONSTRAINT "_SerToTag_A_fkey" FOREIGN KEY ("A") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PictureToSer" ADD CONSTRAINT "_PictureToSer_B_fkey" FOREIGN KEY ("B") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_KeywordToSer" ADD CONSTRAINT "_KeywordToSer_B_fkey" FOREIGN KEY ("B") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSer" ADD CONSTRAINT "_CategoryToSer_B_fkey" FOREIGN KEY ("B") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;
