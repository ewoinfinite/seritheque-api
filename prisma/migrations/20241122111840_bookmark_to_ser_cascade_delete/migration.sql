-- DropForeignKey
ALTER TABLE "BookmarkToSer" DROP CONSTRAINT "BookmarkToSer_serId_fkey";

-- AddForeignKey
ALTER TABLE "BookmarkToSer" ADD CONSTRAINT "BookmarkToSer_serId_fkey" FOREIGN KEY ("serId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;
