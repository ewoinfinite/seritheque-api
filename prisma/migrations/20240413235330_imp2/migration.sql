-- CreateTable
CREATE TABLE "FailedImp" (
    "id" SERIAL NOT NULL,
    "ser_name" TEXT,
    "parent_ser_name" TEXT,
    "source" TEXT,
    "titre" TEXT,
    "categorie" TEXT,
    "description" TEXT,
    "mots_cles1" TEXT,
    "mots_cles2" TEXT,
    "ser_name_no_space" TEXT,

    CONSTRAINT "FailedImp_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "DuplicateImp" (
    "id" SERIAL NOT NULL,
    "ser_name" TEXT,
    "parent_ser_name" TEXT,
    "source" TEXT,
    "titre" TEXT,
    "categorie" TEXT,
    "description" TEXT,
    "mots_cles1" TEXT,
    "mots_cles2" TEXT,
    "ser_name_no_space" TEXT,

    CONSTRAINT "DuplicateImp_pkey" PRIMARY KEY ("id")
);
