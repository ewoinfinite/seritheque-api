/*
  Warnings:

  - Added the required column `excerpt` to the `SerContent` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "SerContent" ADD COLUMN     "excerpt" TEXT NOT NULL;
