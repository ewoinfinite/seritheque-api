-- DropForeignKey
ALTER TABLE "BookmarkToSer" DROP CONSTRAINT "BookmarkToSer_bookmarkId_fkey";

-- AddForeignKey
ALTER TABLE "BookmarkToSer" ADD CONSTRAINT "BookmarkToSer_bookmarkId_fkey" FOREIGN KEY ("bookmarkId") REFERENCES "Bookmark"("id") ON DELETE CASCADE ON UPDATE CASCADE;
