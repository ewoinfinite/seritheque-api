-- CreateExtension
CREATE EXTENSION IF NOT EXISTS "fuzzystrmatch";

-- CreateExtension
CREATE EXTENSION IF NOT EXISTS "pg_trgm";

-- CreateExtension
CREATE EXTENSION IF NOT EXISTS "unaccent";

-- CreateTable
CREATE TABLE "Lang" (
    "id" CHAR(2) NOT NULL,
    "name" VARCHAR(64) NOT NULL,
    "localName" VARCHAR(64) NOT NULL,

    CONSTRAINT "Lang_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Ser" (
    "id" VARCHAR(64) NOT NULL,
    "name" VARCHAR(64) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Ser_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SerContent" (
    "id" SERIAL NOT NULL,
    "serId" VARCHAR(64) NOT NULL,
    "langId" CHAR(2) NOT NULL,
    "title" VARCHAR(512) NOT NULL,
    "content" TEXT NOT NULL,
    "noAccentTitle" VARCHAR(512) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "SerContent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SerEdge" (
    "fromId" VARCHAR(64) NOT NULL,
    "toId" VARCHAR(64) NOT NULL,
    "order" INTEGER,

    CONSTRAINT "SerEdge_pkey" PRIMARY KEY ("fromId","toId")
);

-- CreateTable
CREATE TABLE "SerToSource" (
    "serId" VARCHAR(64) NOT NULL,
    "sourceId" INTEGER NOT NULL,
    "reference" TEXT,
    "description" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "SerToSource_pkey" PRIMARY KEY ("serId","sourceId")
);

-- CreateTable
CREATE TABLE "Picture" (
    "id" SERIAL NOT NULL,
    "path" VARCHAR(512) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Picture_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PictureContent" (
    "pictureId" INTEGER NOT NULL,
    "langId" VARCHAR(2) NOT NULL,
    "title" VARCHAR(512) NOT NULL,
    "description" TEXT,

    CONSTRAINT "PictureContent_pkey" PRIMARY KEY ("pictureId","langId")
);

-- CreateTable
CREATE TABLE "PictureCaptionToSer" (
    "pictureId" INTEGER NOT NULL,
    "serId" VARCHAR(64) NOT NULL,
    "order" VARCHAR(64),
    "orderN" INTEGER,
    "orderA" VARCHAR(64),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "PictureCaptionToSer_pkey" PRIMARY KEY ("serId","pictureId")
);

-- CreateTable
CREATE TABLE "Source" (
    "id" SERIAL NOT NULL,
    "tmpName" VARCHAR(512),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Source_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "SourceContent" (
    "sourceId" INTEGER NOT NULL,
    "langId" CHAR(2) NOT NULL,
    "title" VARCHAR(512) NOT NULL,
    "description" TEXT NOT NULL,
    "isbn" VARCHAR(64),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "SourceContent_pkey" PRIMARY KEY ("langId","sourceId")
);

-- CreateTable
CREATE TABLE "Tag" (
    "id" SERIAL NOT NULL,
    "tmpName" VARCHAR(512),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Tag_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TagContent" (
    "tagId" INTEGER NOT NULL,
    "langId" CHAR(2) NOT NULL,
    "name" VARCHAR(512) NOT NULL,
    "description" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "TagContent_pkey" PRIMARY KEY ("langId","tagId")
);

-- CreateTable
CREATE TABLE "Keyword" (
    "id" SERIAL NOT NULL,
    "tmpName" VARCHAR(512),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Keyword_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "KeywordContent" (
    "keywordId" INTEGER NOT NULL,
    "langId" CHAR(2) NOT NULL,
    "word" VARCHAR(512) NOT NULL,
    "noAccentWord" VARCHAR(512) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "KeywordContent_pkey" PRIMARY KEY ("langId","keywordId")
);

-- CreateTable
CREATE TABLE "Category" (
    "id" SERIAL NOT NULL,
    "parentId" INTEGER,
    "order" INTEGER,
    "tmpName" VARCHAR(512),
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Category_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CategoryContent" (
    "categoryId" INTEGER NOT NULL,
    "langId" CHAR(2) NOT NULL,
    "name" VARCHAR(512) NOT NULL,
    "description" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "CategoryContent_pkey" PRIMARY KEY ("langId","categoryId")
);

-- CreateTable
CREATE TABLE "_SerToTag" (
    "A" VARCHAR(64) NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_PictureToSer" (
    "A" INTEGER NOT NULL,
    "B" VARCHAR(64) NOT NULL
);

-- CreateTable
CREATE TABLE "_KeywordToSer" (
    "A" INTEGER NOT NULL,
    "B" VARCHAR(64) NOT NULL
);

-- CreateTable
CREATE TABLE "_CategoryToSer" (
    "A" INTEGER NOT NULL,
    "B" VARCHAR(64) NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Lang_name_key" ON "Lang"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Ser_name_key" ON "Ser"("name");

-- CreateIndex
CREATE INDEX "SerContent_noAccentTitle_idx" ON "SerContent"("noAccentTitle");

-- CreateIndex
CREATE UNIQUE INDEX "SerContent_title_langId_serId_key" ON "SerContent"("title", "langId", "serId");

-- CreateIndex
CREATE UNIQUE INDEX "SerToSource_serId_key" ON "SerToSource"("serId");

-- CreateIndex
CREATE UNIQUE INDEX "Picture_path_key" ON "Picture"("path");

-- CreateIndex
CREATE UNIQUE INDEX "PictureCaptionToSer_pictureId_order_key" ON "PictureCaptionToSer"("pictureId", "order");

-- CreateIndex
CREATE UNIQUE INDEX "PictureCaptionToSer_pictureId_orderN_orderA_key" ON "PictureCaptionToSer"("pictureId", "orderN", "orderA");

-- CreateIndex
CREATE UNIQUE INDEX "Source_tmpName_key" ON "Source"("tmpName");

-- CreateIndex
CREATE UNIQUE INDEX "SourceContent_langId_title_key" ON "SourceContent"("langId", "title");

-- CreateIndex
CREATE UNIQUE INDEX "SourceContent_langId_isbn_key" ON "SourceContent"("langId", "isbn");

-- CreateIndex
CREATE UNIQUE INDEX "Tag_tmpName_key" ON "Tag"("tmpName");

-- CreateIndex
CREATE UNIQUE INDEX "TagContent_langId_tagId_name_key" ON "TagContent"("langId", "tagId", "name");

-- CreateIndex
CREATE UNIQUE INDEX "Keyword_tmpName_key" ON "Keyword"("tmpName");

-- CreateIndex
CREATE UNIQUE INDEX "KeywordContent_word_key" ON "KeywordContent"("word");

-- CreateIndex
CREATE UNIQUE INDEX "KeywordContent_noAccentWord_key" ON "KeywordContent"("noAccentWord");

-- CreateIndex
CREATE UNIQUE INDEX "Category_tmpName_key" ON "Category"("tmpName");

-- CreateIndex
CREATE INDEX "Category_parentId_order_idx" ON "Category"("parentId", "order");

-- CreateIndex
CREATE UNIQUE INDEX "CategoryContent_name_langId_key" ON "CategoryContent"("name", "langId");

-- CreateIndex
CREATE UNIQUE INDEX "_SerToTag_AB_unique" ON "_SerToTag"("A", "B");

-- CreateIndex
CREATE INDEX "_SerToTag_B_index" ON "_SerToTag"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_PictureToSer_AB_unique" ON "_PictureToSer"("A", "B");

-- CreateIndex
CREATE INDEX "_PictureToSer_B_index" ON "_PictureToSer"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_KeywordToSer_AB_unique" ON "_KeywordToSer"("A", "B");

-- CreateIndex
CREATE INDEX "_KeywordToSer_B_index" ON "_KeywordToSer"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_CategoryToSer_AB_unique" ON "_CategoryToSer"("A", "B");

-- CreateIndex
CREATE INDEX "_CategoryToSer_B_index" ON "_CategoryToSer"("B");

-- AddForeignKey
ALTER TABLE "SerContent" ADD CONSTRAINT "SerContent_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerContent" ADD CONSTRAINT "SerContent_serId_fkey" FOREIGN KEY ("serId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerEdge" ADD CONSTRAINT "SerEdge_fromId_fkey" FOREIGN KEY ("fromId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerEdge" ADD CONSTRAINT "SerEdge_toId_fkey" FOREIGN KEY ("toId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerToSource" ADD CONSTRAINT "SerToSource_serId_fkey" FOREIGN KEY ("serId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SerToSource" ADD CONSTRAINT "SerToSource_sourceId_fkey" FOREIGN KEY ("sourceId") REFERENCES "Source"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PictureContent" ADD CONSTRAINT "PictureContent_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PictureContent" ADD CONSTRAINT "PictureContent_pictureId_fkey" FOREIGN KEY ("pictureId") REFERENCES "Picture"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PictureCaptionToSer" ADD CONSTRAINT "PictureCaptionToSer_pictureId_fkey" FOREIGN KEY ("pictureId") REFERENCES "Picture"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PictureCaptionToSer" ADD CONSTRAINT "PictureCaptionToSer_serId_fkey" FOREIGN KEY ("serId") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SourceContent" ADD CONSTRAINT "SourceContent_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SourceContent" ADD CONSTRAINT "SourceContent_sourceId_fkey" FOREIGN KEY ("sourceId") REFERENCES "Source"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TagContent" ADD CONSTRAINT "TagContent_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TagContent" ADD CONSTRAINT "TagContent_tagId_fkey" FOREIGN KEY ("tagId") REFERENCES "Tag"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "KeywordContent" ADD CONSTRAINT "KeywordContent_keywordId_fkey" FOREIGN KEY ("keywordId") REFERENCES "Keyword"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "KeywordContent" ADD CONSTRAINT "KeywordContent_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Category" ADD CONSTRAINT "Category_parentId_fkey" FOREIGN KEY ("parentId") REFERENCES "Category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CategoryContent" ADD CONSTRAINT "CategoryContent_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CategoryContent" ADD CONSTRAINT "CategoryContent_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SerToTag" ADD CONSTRAINT "_SerToTag_A_fkey" FOREIGN KEY ("A") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_SerToTag" ADD CONSTRAINT "_SerToTag_B_fkey" FOREIGN KEY ("B") REFERENCES "Tag"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PictureToSer" ADD CONSTRAINT "_PictureToSer_A_fkey" FOREIGN KEY ("A") REFERENCES "Picture"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_PictureToSer" ADD CONSTRAINT "_PictureToSer_B_fkey" FOREIGN KEY ("B") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_KeywordToSer" ADD CONSTRAINT "_KeywordToSer_A_fkey" FOREIGN KEY ("A") REFERENCES "Keyword"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_KeywordToSer" ADD CONSTRAINT "_KeywordToSer_B_fkey" FOREIGN KEY ("B") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSer" ADD CONSTRAINT "_CategoryToSer_A_fkey" FOREIGN KEY ("A") REFERENCES "Category"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_CategoryToSer" ADD CONSTRAINT "_CategoryToSer_B_fkey" FOREIGN KEY ("B") REFERENCES "Ser"("id") ON DELETE CASCADE ON UPDATE CASCADE;
