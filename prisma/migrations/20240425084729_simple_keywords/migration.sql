/*
  Warnings:

  - You are about to drop the column `tmpName` on the `Keyword` table. All the data in the column will be lost.
  - You are about to drop the `KeywordContent` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[langId,noAccentWord]` on the table `Keyword` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `langId` to the `Keyword` table without a default value. This is not possible if the table is not empty.
  - Added the required column `noAccentWord` to the `Keyword` table without a default value. This is not possible if the table is not empty.
  - Added the required column `word` to the `Keyword` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "KeywordContent" DROP CONSTRAINT "KeywordContent_keywordId_fkey";

-- DropForeignKey
ALTER TABLE "KeywordContent" DROP CONSTRAINT "KeywordContent_langId_fkey";

-- DropIndex
DROP INDEX "Keyword_tmpName_key";

-- AlterTable
ALTER TABLE "Keyword" DROP COLUMN "tmpName",
ADD COLUMN     "langId" CHAR(2) NOT NULL,
ADD COLUMN     "noAccentWord" VARCHAR(512) NOT NULL,
ADD COLUMN     "word" VARCHAR(512) NOT NULL;

-- DropTable
DROP TABLE "KeywordContent";

-- CreateIndex
CREATE UNIQUE INDEX "Keyword_langId_noAccentWord_key" ON "Keyword"("langId", "noAccentWord");

-- AddForeignKey
ALTER TABLE "Keyword" ADD CONSTRAINT "Keyword_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE CASCADE ON UPDATE CASCADE;
