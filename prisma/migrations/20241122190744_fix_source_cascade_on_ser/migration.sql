-- DropForeignKey
ALTER TABLE "Ser" DROP CONSTRAINT "Ser_sourceId_fkey";

-- AddForeignKey
ALTER TABLE "Ser" ADD CONSTRAINT "Ser_sourceId_fkey" FOREIGN KEY ("sourceId") REFERENCES "Source"("id") ON DELETE SET NULL ON UPDATE CASCADE;
