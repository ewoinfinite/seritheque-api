-- DropForeignKey
ALTER TABLE "User" DROP CONSTRAINT "User_langId_fkey";

-- AlterTable
ALTER TABLE "User" ALTER COLUMN "langId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "User" ADD CONSTRAINT "User_langId_fkey" FOREIGN KEY ("langId") REFERENCES "Lang"("id") ON DELETE SET NULL ON UPDATE CASCADE;
