/*
  Warnings:

  - You are about to drop the `DuplicateImp` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `FailedImp` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "DuplicateImp";

-- DropTable
DROP TABLE "FailedImp";
