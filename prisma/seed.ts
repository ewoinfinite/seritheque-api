import { Prisma, PrismaClient } from '@prisma/client';

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  log: ['warn', 'error'],
});

const defaultLanguages: Prisma.LangCreateInput[] = [
  {
    id: 'fr',
    name: 'French',
    localName: 'Français',
  },
];

export async function seedLanguages() {
  const hasLanguages = await prisma.lang.findFirst();

  if (!hasLanguages) {
    console.log(`Seeding default languages`);

    await prisma.lang.createMany({ data: defaultLanguages });
    console.log(`Done.`);
  }
}

async function seed() {
  await seedLanguages();
}

seed()
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
