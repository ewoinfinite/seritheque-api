import { PrismaClient } from '@prisma/client';

const getOrCreateSource = async (
  title: string | null | undefined,
): Promise<number | undefined> => {
  const tmpTitle = title?.trim();

  if (!tmpTitle) return;

  const source =
    (await prisma.source.findUnique({
      where: { tmpTitle },
    })) ||
    (await prisma.source.create({
      data: {
        tmpTitle,
        contents: { create: { langId: 'fr', title: tmpTitle } },
      },
    }));

  return source?.id;
};

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  log: ['warn', 'error'],
});

async function seedAtlas() {
  console.log(`Copy data from table impatlas to imp2atlas`);

  await prisma.$executeRaw`
INSERT INTO imp2atlas
  (
    id,
    name,  
    source,
    "sourceNb",
    "nb",
    "nameNoSpace"
  )
SELECT 
  id::int,
  REPLACE(name, '''', ''), 
  source,  
  sourcenb::int,
  nb::int,
  REPLACE(REPLACE(name, '''', ''), ' ', '')
FROM impatlas;
`;

  const count = await prisma.imp2atlas.count();
  console.log(`Done. (${count})`);

  let lastId = -1;
  let countImported = 0;

  console.log(`Inserting content to Atlas table...`);

  while (true) {
    let atlas = await prisma.imp2atlas.findFirst({
      where: { id: { gt: lastId } },
      orderBy: { id: 'asc' },
    });

    if (!atlas) {
      console.log(`Done. (${countImported})`);
      break;
    }

    let sourceId = await getOrCreateSource(atlas.source);

    if (!sourceId) {
      console.error(`Missing source for id:${atlas.id}`);
      continue;
    }

    await prisma.atlas
      .create({
        data: {
          id: atlas.id,
          name: atlas.name,
          nameNoSpace: atlas.nameNoSpace,
          sourceId,
          sourceNb: atlas.sourceNb,
          nb: atlas.nb,
        },
      })
      .then((_) => {
        countImported++;
      })
      .catch((err) => {
        console.error(`failed to import imp2atlas.id:${atlas.id}`);
      });

    lastId = atlas.id;
  }
}

seedAtlas();
