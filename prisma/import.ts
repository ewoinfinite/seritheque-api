import { Prisma, PrismaClient, imp2 } from '@prisma/client';
import {
  convertSpacesToSpacesIndentation,
  convertTabIndentationToSpaces,
  fixTxt,
  fixTxtFr,
  md2txt,
  txt2md,
  unAccent,
} from '../src/utils/utils';
import _ from 'lodash';
import { excerptNbWords } from '../src/shared/constants';

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  log: ['warn', 'error'],
});

const dashWithSpace = /\s-|-\s/g;

async function seedImp2() {
  const hasImp2 = await prisma.imp2.findFirst();

  if (hasImp2) return;

  console.log(`Copy data from table imp to imp2 (add an autoincrement id)`);
  await prisma.$executeRaw`
INSERT INTO imp2
(
  ser_name,
  parent_ser_name, 
  source,  
  titre,
  categorie,
  description,
  mots_cles1,
  mots_cles2,
  ser_name_no_space
)
SELECT 
  ser_name,
  REPLACE(parent_ser_name, ' ', ''), 
  source,  
  titre,
  categorie,
  description,
  mots_cles1,
  mots_cles2,
  REPLACE(ser_name, ' ', '')
FROM imp;
`;

  const count = await prisma.imp2.count();
  console.log(`Done. (${count})`);
}

async function seedSerWithChild() {
  let previousChild = 0;
  console.log(`seedSerWithChild: Seeding data from imp2 to Ser...`);

  const lang = await prisma.lang.findUniqueOrThrow({ where: { id: 'fr' } });
  const langId = lang.id;

  let nbImported = 0;
  let nbSkipped = 0;

  while (true) {
    let nextChild = await prisma.imp2.findFirst({
      where: {
        id: { gt: previousChild },
        AND: [
          {
            parent_ser_name: { not: '' },
          },
          {
            parent_ser_name: { not: null },
          },
        ],
      },
      orderBy: { id: 'asc' },
    });

    if (!nextChild) {
      console.log(`Done.`);
      break;
    }

    let imp = await prisma.imp2.findFirst({
      where: { ser_name_no_space: nextChild.parent_ser_name },
      orderBy: {
        parent_ser_name: { sort: 'desc', nulls: 'first' },
      },
    });

    if (!imp) {
      console.log(
        `Could not find parent of ${nextChild.ser_name_no_space} line ${nextChild.id + 1} with parent ${nextChild.parent_ser_name}`,
      );
      continue;
    }

    // console.log(`Found element id:${imp.id}`);
    // try {
    let nameNoSpace = imp.ser_name?.replace(/\s+/g, ''); // remove all spaces

    let hasSer =
      nameNoSpace && (await prisma.ser.findFirst({ where: { nameNoSpace } }));

    if (hasSer) {
      nbSkipped++;
    } else {
      const data = await imp2serData(imp, langId);

      // console.log(`Importing ${data.id}`);
      await prisma.ser
        .create({
          data,
        })
        .then(() => {
          nbImported++;
        })
        .catch((err) => {
          console.log(`Failed: ${JSON.stringify(data)}`);
        });

      // console.log(JSON.stringify(data));
    }
    // } catch (err) {
    //   console.log(JSON.stringify(nextChild));
    //   throw err;
    // }

    previousChild = nextChild.id;
  } // while (previousChild);

  console.log(`Imported ${nbImported}`);
  console.log(`Skipped ${nbSkipped}`);
  console.log(`Done.`);
}

async function seedSer() {
  let lastImportedId = -1;
  console.log(`Seeding data from imp2 to Ser...`);

  const lang = await prisma.lang.findUniqueOrThrow({ where: { id: 'fr' } });
  const langId = lang.id;

  let nbImported = 0;
  let nbSkipped = 0;

  // let inserts = [];

  while (true) {
    let imp = await prisma.imp2.findFirst({
      where: { id: { gt: lastImportedId } },
      orderBy: { id: 'asc' },
    });

    if (!imp) {
      console.log(`Done.`);
      break;
    }

    // console.log(`Found element id:${imp.id}`);

    let nameNoSpace = imp.ser_name?.replace(/\s+/g, ''); // remove all spaces
    let hasSer =
      nameNoSpace && (await prisma.ser.findFirst({ where: { nameNoSpace } }));

    if (hasSer) {
      nbSkipped++;
    } else {
      const data = await imp2serData(imp, langId);

      // console.log(`Importing ${data.id}`);

      await prisma.ser
        .create({
          data,
        })
        .then(() => {
          nbImported++;
        })
        .catch((err) => {
          console.log(`Failed: ${JSON.stringify(data)}`);
        });
    }

    lastImportedId = imp.id;
  } //while (previousChild);

  console.log(`Imported ${nbImported}`);
  console.log(`Skipped ${nbSkipped}`);
  console.log(`Done.`);
}

async function imp2serData(
  imp: imp2,
  langId: string,
): Promise<Prisma.SerCreateInput> {
  let nameNoSpace = imp.ser_name?.replace(/\s+/g, '');

  let name = imp.ser_name?.replace(/\s+/g, ' ').trim();
  let title: string | null = (imp.titre ?? '').replace(/\s+/g, ' ').trim();

  title = fixTxtFr(title);

  title = title || null; // null will crash db insert and that's what we want

  let noAccentTitle = (title && unAccent(title).toLowerCase()) || null;

  let content: string = imp.description ?? '';
  let excerpt: string = '';

  if (content) {
    content = content.replace(/(?<=^[\s#]*)#/gm, '#'); // escape leading hash

    content = convertSpacesToSpacesIndentation(content, 2, 4);

    content = convertTabIndentationToSpaces(content, 4);

    content = content.replace(/^°\s+/gm, '    - '); // sounds like this sign was used for level two

    content = fixTxt(content, 'fr');

    content = txt2md(content);

    const excerptA = md2txt(content).split(/\s/);

    excerpt =
      excerptA.slice(0, excerptNbWords).join(' ') +
      (excerptA.length > excerptNbWords ? '…' : '');

    // see https://dev.to/kamonwan/the-right-way-to-break-string-into-words-in-javascript-3jp6
    // @ts-ignore
    // const segmenter = new Intl.Segmenter([], { granularity: 'word' });
    // const segmentedText = segmenter.segment(excerpt);
    // const words = [...segmentedText].reduce((s) => s,[])
    //   .filter((s) => s.isWordLike)
    //   .map((s) => s.segment);
  }

  let cats = imp.categorie
    ?.split(dashWithSpace)
    .map((name) => name.replace(/\s+/g, ' ').trim());

  /**
   * Recursively create/connect categories
   */

  let categories: Prisma.SerCreateInput['categories'];

  if (cats?.length) {
    let parent: Prisma.CategoryCreateNestedOneWithoutChildrenInput | undefined; //Prisma.SerCreateInput['categories']  // Prisma.CategoryCreateOrConnectWithoutSersInput;

    cats.forEach((cat) => {
      parent = {
        connectOrCreate: {
          where: {
            tmpName: cat,
          },
          create: {
            tmpName: cat,
            contents: { create: { langId, name: cat } },
            parent,
          },
        },
      };
    });

    categories = parent;
  }

  /**
   * Keywords
   */

  let mots_cles1 = imp.mots_cles1?.split(/,/g) || [];
  let mots_cles2 = imp.mots_cles2?.split(/,/g) || [];

  let words: string[] = mots_cles1
    .concat(mots_cles2)
    .map((t) => t.trim().replace(/\s+/g, ' '))
    .filter((t) => t);

  let keywords: Prisma.SerCreateInput['keywords'];

  if (words.length) {
    keywords = {
      connectOrCreate: words.map((word) => {
        const noAccentWord = unAccent(word).toLowerCase();

        const result: Prisma.KeywordCreateOrConnectWithoutSersInput = {
          where: {
            langId_noAccentWord: {
              langId,
              noAccentWord,
            },
          },
          create: {
            langId,
            word,
            noAccentWord,
          },
        };

        return result;
      }),
    };
  }

  /**
   * Sources
   */

  // let serToSources: Prisma.SerCreateInput['serToSources'];
  const sourceId = await getOrCreateSource(imp.source);

  const source = sourceId ? { connect: { id: sourceId } } : undefined;

  // let serSourceTitle = imp.source?.trim();
  // if (serSourceTitle) {
  //   let source = await prisma.source.findUnique({
  //     where: { tmpTitle: serSourceTitle },
  //   });

  //   if (!source) {
  //     source = await prisma.source.create({
  //       data: {
  //         tmpTitle: serSourceTitle,
  //         contents: { create: { langId: 'fr', title: serSourceTitle } },
  //       },
  //     });
  //   }

  //   // serToSources = {
  //   //   create: { sourceId: source.id },
  //   // };
  //   sourceId = source.id;
  // }

  if (!nameNoSpace) {
    throw new Error(`nameNoSpace is null (imp2.id:${imp.id})`);
  }

  if (!name) {
    throw new Error(`name is null (imp2.id:${imp.id})`);
  }

  if (!title || !noAccentTitle) {
    throw new Error(`id is null (imp2.id:${imp.id})`);
  }

  const data: Prisma.SerCreateInput = {
    nameNoSpace,
    name,
    contents: {
      create: {
        langId,
        title,
        noAccentTitle,
        content,
        excerpt,
      },
    },
    categories,
    keywords,
    source,
  };

  return data;
}

const getOrCreateSource = async (
  title: string | null | undefined,
): Promise<number | undefined> => {
  const tmpTitle = title?.trim();

  if (!tmpTitle) return;

  const source =
    (await prisma.source.findUnique({
      where: { tmpTitle },
    })) ||
    (await prisma.source.create({
      data: {
        tmpTitle,
        contents: { create: { langId: 'fr', title: tmpTitle } },
      },
    }));

  return source?.id;
};

async function seedSerEdges() {
  let revEdges: Prisma.SerCreateInput['revEdges'];
  let nbConnected = 0;

  console.log(`Connecting Ser edges...`);

  const imps = await prisma.imp2.findMany({
    where: {
      AND: [
        {
          parent_ser_name: { not: '' },
        },
        {
          parent_ser_name: { not: null },
        },
      ],
    },
    orderBy: { id: 'asc' },
  });

  for (let imp of imps) {
    if (!imp.parent_ser_name) {
      throw new Error(`imp2.id:${imp.id} has missing parent_ser_name`);
    }
    if (!imp.ser_name_no_space) {
      throw new Error(`imp2.id:${imp.id} has missing ser_name_no_space`);
    }

    let parentSer = await prisma.ser.findFirstOrThrow({
      where: { nameNoSpace: imp.parent_ser_name },
    });
    let ser = await prisma.ser.findFirstOrThrow({
      where: { nameNoSpace: imp.ser_name_no_space },
    });

    revEdges = {
      connectOrCreate: {
        where: {
          fromId_toId: {
            fromId: parentSer.id,
            toId: ser.id,
          },
        },
        create: {
          fromId: parentSer.id,
        },
      },
    };

    try {
      await prisma.ser.update({
        where: { id: ser.id },
        data: { revEdges },
      });
      nbConnected++;
    } catch (err) {
      console.log(JSON.stringify(imp));

      // throw err;
    }
  }

  console.log(`Connected ${nbConnected}`);
  console.log(`Done.`);
}

async function seed() {
  await seedImp2();

  await seedSerWithChild();
  await seedSer();
  await seedSerEdges();
}

seed()
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
