import { ApiProperty } from '@nestjs/swagger';

export class SimpleCategoryDto {
  @ApiProperty()
  id: number;

  @ApiProperty({ type: Number, nullable: true })
  parentId: number | null;

  @ApiProperty({ type: Number, nullable: true })
  order: number | null;

  @ApiProperty()
  name: string;
}
