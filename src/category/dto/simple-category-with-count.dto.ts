import { ApiProperty } from '@nestjs/swagger';
import { SimpleCategoryDto } from './simple-category.dto';

export class SimpleCategoryWithCountDto extends SimpleCategoryDto {
  @ApiProperty()
  count: number;
}
