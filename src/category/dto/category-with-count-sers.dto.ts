import { ApiProperty } from '@nestjs/swagger';
import { CategoryEntity } from '../entities/category.entity';

export class CategoryWithCountSers extends CategoryEntity {
  _count: {
    sers: number;
  };
}
