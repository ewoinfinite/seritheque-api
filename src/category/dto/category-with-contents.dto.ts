import { ApiProperty } from '@nestjs/swagger';
import { CategoryContentEntity } from '../entities/category-content.entity';
import { CategoryEntity } from '../entities/category.entity';

export class CategoryWithContentsDto extends CategoryEntity {
  @ApiProperty({
    type: CategoryContentEntity,
    isArray: true,
  })
  contents: CategoryContentEntity[];
}
