import { ApiProperty } from '@nestjs/swagger';
import { Category } from '@prisma/client';

export class CategoryEntity implements Category {
  @ApiProperty()
  id: number;

  @ApiProperty({ type: Number, nullable: true })
  parentId: number | null;

  @ApiProperty({ type: Number, nullable: true })
  order: number | null;

  @ApiProperty({ type: String, nullable: true })
  tmpName: string | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
