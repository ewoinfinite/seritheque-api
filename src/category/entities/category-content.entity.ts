import { ApiProperty } from '@nestjs/swagger';
import { CategoryContent } from '@prisma/client';

export class CategoryContentEntity implements CategoryContent {
  @ApiProperty()
  categoryId: number;

  @ApiProperty()
  langId: string;

  @ApiProperty()
  name: string;

  @ApiProperty({ type: String, nullable: true })
  description: string | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
