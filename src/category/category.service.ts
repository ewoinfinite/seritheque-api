import { Injectable } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { CategoryWithContentsDto } from './dto/category-with-contents.dto';
import { CategoryFindManyQuery } from './queries/category-find-many.query';
import { ApiTags } from '@nestjs/swagger';
import { SimpleCategoryDto } from './dto/simple-category.dto';
import { UtilsService } from 'src/utils/utils.service';
import { CategoryFindManySimpleQuery } from './queries/category-find-many-simple.query ';
import { SimpleCategoryWithCountDto } from './dto/simple-category-with-count.dto';
import { count } from 'console';

@ApiTags('Category')
@Injectable()
export class CategoryService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly utils: UtilsService,
  ) {}

  create(createCategoryDto: CreateCategoryDto) {
    return 'This action adds a new category';
  }

  async findMany(
    query: CategoryFindManyQuery,
  ): Promise<CategoryWithContentsDto[]> {
    const langId = query.langId ?? undefined;

    return this.prisma.category.findMany({
      include: {
        contents: {
          where: { langId },
        },
      },
    });
  }

  async findManySimple(
    langId: string,
    query: CategoryFindManySimpleQuery,
  ): Promise<SimpleCategoryWithCountDto[]> {
    const categoriesWithContents = await this.prisma.category.findMany({
      include: {
        _count: { select: { sers: true } },
        contents: {
          where: { langId },
        },
      },
    });

    const result = categoriesWithContents.map((category) => {
      const simpleCategory =
        this.utils.categoryWithContentsToCategorySimple(category);

      const result: SimpleCategoryWithCountDto = {
        ...simpleCategory,
        count: category._count.sers,
      };

      return result;
    });

    return result;
  }

  findOne(id: number) {
    return `This action returns a #${id} category`;
  }

  update(id: number, updateCategoryDto: UpdateCategoryDto) {
    return `This action updates a #${id} category`;
  }

  remove(id: number) {
    return `This action removes a #${id} category`;
  }
}
