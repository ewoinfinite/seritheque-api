import { ApiProperty, OmitType } from '@nestjs/swagger';
import { IsOptional, IsString, Length } from 'class-validator';
import { CategoryFindManySimpleQuery } from './category-find-many-simple.query ';

export class CategoryFindManyQuery {
  @ApiProperty({
    required: false,
    minLength: 2,
    maxLength: 2,
  })
  @IsOptional()
  @IsString()
  @Length(2, 2)
  langId?: string;
}
