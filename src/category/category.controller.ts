import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryWithContentsDto } from './dto/category-with-contents.dto';
import { CategoryFindManyQuery } from './queries/category-find-many.query';
import { SimpleCategoryDto } from './dto/simple-category.dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CategoryFindManySimpleQuery } from './queries/category-find-many-simple.query ';

@Controller('category')
@ApiTags('Category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Post()
  create(@Body() createCategoryDto: CreateCategoryDto) {
    return this.categoryService.create(createCategoryDto);
  }

  @ApiOkResponse({ type: CategoryWithContentsDto, isArray: true })
  @Get()
  findMany(
    @Query() query: CategoryFindManyQuery,
  ): Promise<CategoryWithContentsDto[]> {
    return this.categoryService.findMany(query);
  }

  @ApiOkResponse({ type: SimpleCategoryDto, isArray: true })
  @Get('simple/:langId')
  findManySimple(
    @Param('langId') langId: string,
    @Query() query: CategoryFindManySimpleQuery,
  ): Promise<SimpleCategoryDto[]> {
    return this.categoryService.findManySimple(langId, query);
  }

  @Get('raw/:id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.categoryService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCategoryDto: UpdateCategoryDto,
  ) {
    return this.categoryService.update(+id, updateCategoryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.categoryService.remove(+id);
  }
}
