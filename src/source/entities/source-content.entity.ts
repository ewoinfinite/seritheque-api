import { ApiProperty } from '@nestjs/swagger';
import { SourceContent } from '@prisma/client';

export class SourceContentEntity implements SourceContent {
  @ApiProperty()
  sourceId: number;

  @ApiProperty()
  langId: string;

  @ApiProperty()
  title: string;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  description: string | null;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  isbn: string | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
