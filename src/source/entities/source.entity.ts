import { ApiProperty } from '@nestjs/swagger';
import { Source } from '@prisma/client';

export class SourceEntity implements Source {
  @ApiProperty()
  id: number;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  tmpTitle: string | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty({ nullable: true, type: Number })
  order: number | null;
}
