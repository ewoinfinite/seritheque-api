import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class SourceFindManyQuery {
  @ApiProperty({
    type: String,
    required: false,
    default: 'fr',
  })
  @IsOptional()
  @IsString()
  langId?: string;
}
