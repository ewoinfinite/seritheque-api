import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { SourceService } from './source.service';
import { CreateSourceDto } from './dto/create-source.dto';
import { UpdateSourceDto } from './dto/update-source.dto';
import { Source } from '@prisma/client';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { SourceEntity } from './entities/source.entity';
import { SourceWithContentsDto } from './dto/source-with-contents.dto';
import { SourceFindManyQuery } from './queries/source-find-many.query';
import { SimpleSourceDto } from './dto/simple-source.dto';

@Controller('source')
@ApiTags('Source')
export class SourceController {
  constructor(private readonly sourceService: SourceService) {}

  // @Post()
  // create(@Body() createSourceDto: CreateSourceDto) {
  //   return this.sourceService.create(createSourceDto);
  // }

  @Get()
  @ApiOkResponse({
    type: SourceWithContentsDto,
    isArray: true,
  })
  async findAll(
    @Query() query: SourceFindManyQuery,
  ): Promise<SourceWithContentsDto[]> {
    return this.sourceService.findAll(query);
  }

  @Get('simple')
  @ApiOkResponse({
    type: SimpleSourceDto,
    isArray: true,
  })
  async findAllSimple(
    @Query() query: SourceFindManyQuery,
  ): Promise<SimpleSourceDto[]> {
    return this.sourceService.findAll(query).then((sources) =>
      sources.map(
        (source): SimpleSourceDto => ({
          id: source.id,
          title: source.contents[0]?.title ?? source.tmpTitle,
          isbn: source.contents[0]?.isbn ?? null,
          description: source.contents[0]?.description ?? null,
        }),
      ),
    );
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.sourceService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateSourceDto: UpdateSourceDto) {
  //   return this.sourceService.update(+id, updateSourceDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.sourceService.remove(+id);
  // }
}
