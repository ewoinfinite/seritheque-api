import { Injectable } from '@nestjs/common';
import { CreateSourceDto } from './dto/create-source.dto';
import { UpdateSourceDto } from './dto/update-source.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { Source } from '@prisma/client';
import { SourceWithContentsDto } from './dto/source-with-contents.dto';
import { SourceFindManyQuery } from './queries/source-find-many.query';

@Injectable()
export class SourceService {
  constructor(private readonly prisma: PrismaService) {}
  create(createSourceDto: CreateSourceDto) {
    return 'This action adds a new source';
  }

  async findAll(query: SourceFindManyQuery): Promise<SourceWithContentsDto[]> {
    return this.prisma.source.findMany({
      // select: {
      //   id: true,
      //   tmpTitle: true,
      //   createdAt: true,
      //   updatedAt: true,
      //   order: true,

      //   contents: {
      //     where: {
      //       langId: query.langId ?? 'fr',
      //     },
      //     // orderBy: { langId: 'asc' },
      //   },
      //   _count: { select: { sers: true } },
      //   // sers: { select: { _count: true } },
      // },

      include: {
        contents: { where: { langId: query.langId ?? 'fr' } },
        _count: { select: { sers: true } },
      },
      orderBy: { sers: { _count: 'desc' } },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} source`;
  }

  update(id: number, updateSourceDto: UpdateSourceDto) {
    return `This action updates a #${id} source`;
  }

  remove(id: number) {
    return `This action removes a #${id} source`;
  }
}
