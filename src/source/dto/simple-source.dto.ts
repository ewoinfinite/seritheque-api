import { ApiProperty } from '@nestjs/swagger';

export class SimpleSourceDto {
  @ApiProperty()
  id: number;

  // @ApiProperty()
  // langId: string;

  @ApiProperty()
  title: string;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  description: string | null;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  isbn: string | null;
}
