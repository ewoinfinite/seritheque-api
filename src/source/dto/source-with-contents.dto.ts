import { ApiProperty } from '@nestjs/swagger';
import { SourceEntity } from '../entities/source.entity';
import { SourceContentEntity } from '../entities/source-content.entity';

export class SourceWithContentsDto extends SourceEntity {
  @ApiProperty({
    type: SourceContentEntity,
    isArray: true,
  })
  contents: SourceContentEntity[];

  @ApiProperty({
    type: new (class {
      _count: { sers: number };
    })(),
    readOnly: true,
    required: false,
  })
  _count?: { sers: number };
}
