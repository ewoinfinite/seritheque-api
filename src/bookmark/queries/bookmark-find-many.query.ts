import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsNotEmpty } from 'class-validator';

export class BookmarkFindManyQuery {
  @ApiProperty({
    type: String,
    required: false,
    example: 'fr',
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  langId?: string;
}
