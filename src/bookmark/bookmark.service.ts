import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotAcceptableException,
} from '@nestjs/common';
import { CreateBookmarkDto } from './dto/create-bookmark.dto';
import { UpdateBookmarkDto } from './dto/update-bookmark.dto';
import { ReqUserDto } from 'src/auth/dto/req-user.dto';
import { Bookmark, BookmarkToSer, Prisma, Ser } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { SerWithContentsDto } from 'src/ser/dto/ser-with-contents.dto';

@Injectable()
export class BookmarkService {
  private readonly logger = new Logger('BookmarkService');

  constructor(private readonly prisma: PrismaService) {}

  async create(
    createBookmarkDto: CreateBookmarkDto,
    userId: string,
  ): Promise<Bookmark> {
    return this.prisma.bookmark
      .create({
        data: { ...createBookmarkDto, userId },
      })
      .catch((error) => {
        if (
          error instanceof Prisma.PrismaClientKnownRequestError &&
          error.code === 'P2002'
        ) {
          throw new BadRequestException(
            'Bookmark with this label already exist',
          );
        }

        this.logger.error(error.message, error);
        throw new InternalServerErrorException();
      });
  }

  async connectSer(userId: string, serId: number, label: string = 'default') {
    label = label.trim();
    if (!label) {
      throw new NotAcceptableException(`Label can not be empty`);
    }

    const bookmark =
      (await this.prisma.bookmark.findUnique({
        where: { userId_label: { userId, label } },
      })) || (await this.prisma.bookmark.create({ data: { userId, label } }));

    return this.prisma.bookmark.update({
      where: { userId_label: { userId, label } },
      data: {
        bookmarkToSers: {
          // connect: { bookmarkId_serId: { serId, bookmarkId: bookmark.id } },
          // create: { serId },
          connectOrCreate: {
            where: { bookmarkId_serId: { serId, bookmarkId: bookmark.id } },
            create: { serId },
          },
        },
      },
    });
  }

  async disconnectSer(
    userId: string,
    serId: number,
    label: string = 'default',
  ) {
    label = label.trim();
    if (!label) {
      throw new NotAcceptableException(`Label can not be empty`);
    }

    const bookmark = await this.prisma.bookmark.findUnique({
      where: { userId_label: { userId, label } },
    });

    if (!bookmark) {
      throw new NotAcceptableException(
        `Bookmark label '${label}' doesn't exist`,
      );
    }

    // return this.prisma.bookmark.update({
    //   where: { userId_label: { userId, label } },
    //   data: {
    //     bookmarkToSers: {
    //       disconnect: { bookmarkId_serId: { serId, bookmarkId: bookmark.id } },
    //     },
    //   },
    // });

    return this.prisma.bookmarkToSer.delete({
      where: { bookmarkId_serId: { serId, bookmarkId: bookmark.id } },
    });
  }

  async findMany(
    userId: string,
    langId: string = 'fr',
  ): Promise<
    (Bookmark & {
      bookmarkToSers: (BookmarkToSer & { ser: SerWithContentsDto })[];
    })[]
  > {
    return this.prisma.bookmark.findMany({
      where: { userId },
      include: {
        bookmarkToSers: {
          include: {
            ser: {
              include: { contents: { where: { langId } } },
            },
          },
          where: { ser: { contents: { some: { langId } } } },
          orderBy: { createdAt: 'desc' },
        },
      },
      orderBy: { label: 'asc' },
      // select: {
      //   id: true,
      //   userId: true,
      //   label: true,
      //   createdAt: true,
      //   updatedAt: true,
      //   sers: true,
      // },
    });
  }

  // findOne(id: number) {
  //   return `This action returns a #${id} bookmark`;
  // }

  async update(
    userId: string,
    label: string,
    updateBookmarkDto: UpdateBookmarkDto,
  ): Promise<Bookmark> {
    const checkUserMatch = await this.prisma.bookmark.findUnique({
      where: { userId_label: { userId, label } },
    });
    if (!checkUserMatch) {
      throw new NotAcceptableException(
        `Bookmark doesn't exist (label:${label} userId:${userId})`,
      );
    }

    return this.prisma.bookmark.update({
      where: { userId_label: { userId, label } },
      data: updateBookmarkDto,
    });
  }

  async remove(userId: string, label: string): Promise<Bookmark> {
    return this.prisma.bookmark.delete({
      where: { userId_label: { userId, label } },
    });
  }
}
