import { ApiProperty } from '@nestjs/swagger';
import { Bookmark } from '@prisma/client';

export class BookmarkEntity implements Bookmark {
  @ApiProperty()
  id: number;

  @ApiProperty({ type: String, format: 'uuid' })
  userId: string;

  @ApiProperty()
  label: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
