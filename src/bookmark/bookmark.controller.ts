import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Header,
  Req,
  ParseIntPipe,
  Query,
  ValidationPipe,
} from '@nestjs/common';
import { BookmarkService } from './bookmark.service';
import { CreateBookmarkDto } from './dto/create-bookmark.dto';
import { UpdateBookmarkDto } from './dto/update-bookmark.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ReqUserDto } from 'src/auth/dto/req-user.dto';
import { Bookmark } from '@prisma/client';
import { BookmarkEntity } from './entities/bookmark.entity';
import { BookmarkFindManyQuery } from './queries/bookmark-find-many.query';

@ApiTags('Bookmark')
@Controller('bookmark')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
export class BookmarkController {
  constructor(private readonly bookmarkService: BookmarkService) {}

  @Post()
  async create(
    @Req() req: Request & { user: ReqUserDto },
    @Body() createBookmarkDto: CreateBookmarkDto,
  ): Promise<BookmarkEntity> {
    return this.bookmarkService.create(createBookmarkDto, req.user.id);
  }

  @Post(':label/:serId')
  async connect(
    @Req() req: Request & { user: ReqUserDto },
    @Param('label') label: string,
    @Param('serId', ParseIntPipe) serId: number,
  ) {
    await this.bookmarkService.connectSer(req.user.id, serId, label);

    return { success: true };
  }

  @Delete(':label/:serId')
  async disconnectSer(
    @Req() req: Request & { user: ReqUserDto },
    @Param('label') label: string,
    @Param('serId', ParseIntPipe) serId: number,
  ) {
    await this.bookmarkService.disconnectSer(req.user.id, serId, label);

    return { success: true };
  }

  @Get()
  @Header('Cache-Control', 'no-cache, no-store')
  findMany(
    @Req() req: Request & { user: ReqUserDto },
    @Query(new ValidationPipe({ transform: true }))
    query: BookmarkFindManyQuery,
  ) {
    return this.bookmarkService.findMany(
      req.user.id,
      query.langId || undefined,
    );
  }

  // @Get(':id')
  // @Header('Cache-Control', 'no-cache, no-store')
  // findOne(@Param('id') id: string) {
  //   return this.bookmarkService.findOne(+id);
  // }

  @Patch(':label')
  update(
    @Req() req: Request & { user: ReqUserDto },
    @Param('label') label: string,
    @Body() updateBookmarkDto: UpdateBookmarkDto,
  ) {
    return this.bookmarkService.update(req.user.id, label, updateBookmarkDto);
  }

  @Delete(':label')
  remove(
    @Req() req: Request & { user: ReqUserDto },
    @Param('label') label: string,
  ) {
    return this.bookmarkService.remove(req.user.id, label);
  }
}
