import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class CreateBookmarkDto {
  // @ApiProperty({ type: String, format: 'uuid' })
  // @IsUUID()
  // userId: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  label: string;
}
