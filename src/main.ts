import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  Logger,
  ValidationPipe,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { json, urlencoded } from 'express';

async function bootstrap() {
  const logger = new Logger('main');
  const app = await NestFactory.create(AppModule);

  const configService: ConfigService = app.get(ConfigService);
  const PORT = Number(configService.get('PORT')) || 3000;

  const GLOBAL_PREFIX = configService.get('GLOBAL_PREFIX') || undefined;
  if (GLOBAL_PREFIX) app.setGlobalPrefix(GLOBAL_PREFIX);

  const corsOrigin = configService.get('CORS_ORIGIN') || '*';

  app.enableCors({
    // origin: ['https://www.grigori-grabovoi.academy'],
    origin: corsOrigin === '*' ? corsOrigin : corsOrigin.split(','),
    // allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept',
    // credentials: true, // required for GraphQL
    maxAge: 3600,
  });

  // app.use(json({ limit: '50mb' }));
  // app.use(urlencoded({ extended: true, limit: '50mb' }));

  // Allow custom class-validator ValidationPipes to use Nest Injections
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  // Validate query/params
  app.useGlobalPipes(
    new ValidationPipe({
      always: true,
      whitelist: true,
      forbidNonWhitelisted: true,
      stopAtFirstError: false,
      transform: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  );

  // Validate output data
  app.useGlobalInterceptors(
    new ClassSerializerInterceptor(app.get(Reflector), {
      strategy: 'exposeAll',
      // excludeExtraneousValues: true,
    }),
  );

  const swaggerConfig = new DocumentBuilder()
    .setTitle(`Series Server`)
    .setVersion(process.env.npm_package_version || '0')
    .addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT' })
    .addOAuth2()
    .build();

  const swaggerMountPoint = [GLOBAL_PREFIX, 'swagger']
    .filter((v) => v)
    .join('/');

  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup(swaggerMountPoint, app, document);

  await app.listen(PORT).then(() => {
    logger.log(
      `app listening on http://localhost:${PORT}/${GLOBAL_PREFIX || ''}`,
    );
    logger.log(`swagger http://localhost:${PORT}/${swaggerMountPoint}`);
  });
}
bootstrap();
