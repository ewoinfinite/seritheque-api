import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsInt, Min, Max } from 'class-validator';
import { PaginateOptions } from './prisma-paginator';

export class PaginateOptionsDto implements PaginateOptions {
  @ApiPropertyOptional({
    type: Number,
  })
  @IsOptional()
  @IsInt()
  @Min(0)
  page?: number;

  @ApiPropertyOptional({
    type: Number,
    default: 50,
    maximum: 500,
    minimum: 1,
  })
  @IsOptional()
  @IsInt()
  @Max(500)
  @Min(1)
  perPage?: number;
}
