import { ApiProperty } from '@nestjs/swagger';
import { PaginatedResult } from './prisma-paginator';
import { paginatorPerPageDefault } from 'src/shared/constants';

type PaginatedResultMeta = PaginatedResult<unknown>['meta'];

export class PaginatedResponseMetaDto implements PaginatedResultMeta {
  @ApiProperty({
    type: Number,
  })
  total: number;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  lastPage: number;

  @ApiProperty({
    type: Number,
    default: 1,
  })
  currentPage: number;

  @ApiProperty({
    type: Number,
    default: paginatorPerPageDefault,
  })
  perPage: number;

  @ApiProperty({
    type: Number,
    nullable: true,
    default: null,
  })
  prev: number | null;

  @ApiProperty({
    type: Number,
    nullable: true,
    default: null,
  })
  next: number | null;
}

export class PaginatedResponseDto<T = unknown>
  implements PaginatedResult<unknown>
{
  @ApiProperty({
    type: Object,
    isArray: true,
  })
  data: T[];

  @ApiProperty({
    type: PaginatedResponseMetaDto,
  })
  meta: PaginatedResponseMetaDto;
}
