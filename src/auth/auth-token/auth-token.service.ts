import { Injectable } from '@nestjs/common';
import { AuthToken, Prisma, User } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class AuthTokenService {
  constructor(private readonly prisma: PrismaService) {}

  async create(user: User, userAgent: string): Promise<AuthToken> {
    const expiresAt: Date | null = user.expiresAt;
    const userId: string = user.id;

    return this.prisma.authToken.create({
      data: {
        userId,
        expiresAt,
        userAgent,
      },
    });
  }

  async getUserAuthTokens(userId: string): Promise<AuthToken[]> {
    return this.prisma.authToken.findMany({
      where: {
        userId,
        OR: [{ expiresAt: null }, { expiresAt: { lt: new Date() } }],
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
  }

  async deleteOne(id: string, userId?: string): Promise<AuthToken> {
    return this.prisma.authToken.delete({
      where: {
        id,
        userId,
      },
    });
  }

  async deleteAllUserTokens(
    userId: string,
    excludeAuthTokenId: string | string[] = [],
  ): Promise<Prisma.BatchPayload> {
    excludeAuthTokenId = Array.isArray(excludeAuthTokenId)
      ? excludeAuthTokenId
      : [excludeAuthTokenId];

    return this.prisma.authToken.deleteMany({
      where: {
        userId: userId,
        id: {
          notIn: excludeAuthTokenId,
        },
      },
    });
  }
}
