import {
  Controller,
  Get,
  UseGuards,
  Request,
  Param,
  Delete,
  ParseUUIDPipe,
  Header,
  Req,
} from '@nestjs/common';
import { AuthTokenService } from './auth-token.service';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { AuthToken } from '@prisma/client';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { AuthTokenEntity } from '../entities/auth-token.entity';
import { ReqUserDto } from '../dto/req-user.dto';

@Controller('auth/token')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('Auth')
export class AuthTokenController {
  constructor(private readonly authTokenService: AuthTokenService) {}
  @Get('')
  @Header('Cache-Control', 'no-cache, no-store')
  @ApiOperation({
    summary: `Return all active authTokens of currently logged-in user`,
  })
  @ApiOkResponse({
    type: AuthTokenEntity,
    isArray: true,
  })
  async getCurrentUserTokens(
    @Request() req: Express.Request & { user: ReqUserDto },
  ): Promise<AuthTokenEntity[]> {
    return this.authTokenService.getUserAuthTokens(req.user.id);
  }

  @Delete(':id')
  @ApiOkResponse({
    type: AuthTokenEntity,
  })
  async deleteOne(
    @Param('id', ParseUUIDPipe) id: string,
    @Req() req: Express.Request & { user: ReqUserDto },
  ): Promise<AuthToken> {
    return this.authTokenService.deleteOne(id, req.user.id);
  }
}
