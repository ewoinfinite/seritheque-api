import { Test, TestingModule } from '@nestjs/testing';
import { AuthTokenController } from './auth-token.controller';
import { AuthTokenService } from './auth-token.service';
import { PrismaModule } from 'src/prisma/prisma.module';

describe('AuthTokenController', () => {
  let controller: AuthTokenController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      controllers: [AuthTokenController],
      providers: [AuthTokenService],
    }).compile();

    controller = module.get<AuthTokenController>(AuthTokenController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
