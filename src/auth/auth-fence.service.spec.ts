import { Test, TestingModule } from '@nestjs/testing';
import { AuthFenceService } from './auth-fence.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { AuthModule } from './auth.module';

describe('AuthFenceService', () => {
  let service: AuthFenceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule, AuthModule],
      providers: [AuthFenceService],
    }).compile();

    service = module.get<AuthFenceService>(AuthFenceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
