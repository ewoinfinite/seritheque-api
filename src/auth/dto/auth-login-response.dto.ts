import { ApiProperty } from '@nestjs/swagger';

export class AuthLoginResponseDto {
  @ApiProperty({
    description: `JWT token`,
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN0cmluZyIsInN1YiI6ImVmNjQ1Y2ZjLWM4NWYtNDRlMy1iN2I4LTcwMmI3ZGE0NTgxYiIsImlhdCI6MTY3NTg0OTY2OCwiZXhwIjoxNjgxMDMzNjY4fQ.CcNBG8zVeDnWXWQHaVcQu_JgDSTmyGmSSRKdPPaGBHo',
  })
  token: string;
}
