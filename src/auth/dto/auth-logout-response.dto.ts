import { ApiProperty } from '@nestjs/swagger';

export class AuthLogoutResponseDto {
  @ApiProperty()
  success: boolean;

  @ApiProperty({
    default: `Successfully logged out`,
  })
  message: string;
}
