import { ApiProperty, OmitType } from '@nestjs/swagger';
import { User, UserRoleEnum } from '@prisma/client';
import { IsUUID, IsString } from 'class-validator';
import { AuthTokenEntity } from '../entities/auth-token.entity';
import { LangEntity } from 'src/lang/entities/lang.entity';

export class ReqUserAuthTokenDto extends OmitType(AuthTokenEntity, [
  'userId',
  'userAgent',
]) {}

export class ReqUserDto implements Partial<User> {
  @ApiProperty({ type: String, format: 'uuid' })
  @IsUUID(4)
  id: string;

  @ApiProperty({ type: ReqUserAuthTokenDto })
  authToken: ReqUserAuthTokenDto;

  @ApiProperty({ type: String })
  @IsString()
  name: string;

  @ApiProperty()
  email: string;

  @ApiProperty({ nullable: true })
  langId: string | null;

  @ApiProperty({ type: LangEntity, nullable: true })
  lang?: LangEntity | null;

  @ApiProperty({ type: Boolean })
  isSuperAdmin?: boolean;

  @ApiProperty({
    enum: UserRoleEnum,
  })
  role: UserRoleEnum;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  provider: string | null;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  providerId: string | null;

  // @ApiProperty({
  //   type: RoleEntity,
  //   isArray: true,
  // })
  // roles: RoleEntity[];

  // @ApiProperty({
  //   type: LanguageEntity,
  //   isArray: true,
  // })
  // translatorLanguages: LanguageEntity[];
}
