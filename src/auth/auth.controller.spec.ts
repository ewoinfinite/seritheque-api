import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthTokenService } from './auth-token/auth-token.service';
import { JwtService } from '@nestjs/jwt';
import { PasswordService } from './password.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UserModule } from 'src/user/user.module';
import { AuthTokenModule } from './auth-token/auth-token.module';
import { AuthFenceService } from './auth-fence.service';

describe('AuthController', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule, UserModule, AuthTokenModule],
      controllers: [AuthController],
      providers: [
        AuthService,
        AuthFenceService,
        JwtService,
        AuthTokenService,
        PasswordService,
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
