import { SetMetadata } from '@nestjs/common';

export const WooMembershipPlan = (...args: number[]) =>
  SetMetadata('woo-membership-plan', args);
