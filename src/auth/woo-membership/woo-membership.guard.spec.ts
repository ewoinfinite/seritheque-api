import { WooMembershipGuard } from './woo-membership.guard';

describe('WooMembershipGuard', () => {
  it('should be defined', () => {
    expect(new WooMembershipGuard()).toBeDefined();
  });
});
