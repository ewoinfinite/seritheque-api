import { Test, TestingModule } from '@nestjs/testing';
import { WooMembershipService } from './woo-membership.service';

describe('WooMembershipService', () => {
  let service: WooMembershipService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WooMembershipService],
    }).compile();

    service = module.get<WooMembershipService>(WooMembershipService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
