import { Injectable } from '@nestjs/common';
import { WooMembershipMember } from './woo-membership-member.interface';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';

@Injectable()
export class WooMembershipService {
  constructor(
    // private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {}

  async getUserMemberships(userId: string): Promise<WooMembershipMember[]> {
    const url = this.configService.getOrThrow<string>(
      'WP_WOO_MEMBERSHIPS_MEMBERS_URL',
    );
    const username = this.configService.getOrThrow<string>('WP_WOO_CLIENT_KEY');
    const password = this.configService.getOrThrow<string>(
      'WP_WOO_CLIENT_SECRET',
    );

    return axios
      .get<
        WooMembershipMember[]
      >(`${url}?customer=${userId}`, { auth: { username, password } })
      .then((response) => response.data);
  }

  matchingMemberships(
    userMemberships: WooMembershipMember[],
    planIds: number[],
  ): WooMembershipMember[] {
    return userMemberships.filter(
      (userMembership) =>
        planIds.includes(userMembership.plan_id) &&
        userMembership.status === 'active',
    );
  }
}
