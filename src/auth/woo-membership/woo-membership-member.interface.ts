/**
 * https://woocommerce.github.io/subscriptions-rest-api-docs/ - There I found enums of status
 * https://godaddy-wordpress.github.io/woocommerce-memberships-rest-api-docs/#user-memberships
 */

export interface WooMembershipMember {
  id: number;
  customer_id: number;
  customer_data: any[];
  plan_id: number;
  plan_name: string;
  status:
    | 'pending'
    | 'active'
    | 'on-hold'
    | 'expired'
    | 'pending-cancel'
    | 'cancelled';
  order_id: number | null;
  product_id: number | null;
  subscription_id: number | null;
  date_created: string;
  date_created_gmt: string;
  start_date: string;
  start_date_gmt: string;
  end_date: string | null;
  end_date_gmt: string | null;
  paused_date: string | null;
  paused_date_gmt: string | null;
  cancelled_date: string | null;
  cancelled_date_gmt: string | null;
  view_url: string; // "https://www2.grigori-grabovoi.academy/my-account-2/members-area/23231/my-membership-content/",
  profile_fields: any[];
  meta_data: any[];
  _links: {
    self: [
      {
        href: string; // "https://www2.grigori-grabovoi.academy/wp-json/wc/v3/memberships/members/23264"
      },
    ];
    collection: [
      {
        href: string; //"https://www2.grigori-grabovoi.academy/wp-json/wc/v3/memberships/members"
      },
    ];
    customer: [
      {
        href: string; // "https://www2.grigori-grabovoi.academy/wp-json/wc/v3/customers/3"
      },
    ];
  };
}
