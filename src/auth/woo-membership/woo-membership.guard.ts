import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { ReqUserDto } from '../dto/req-user.dto';
import { WooMembershipService } from './woo-membership.service';
import { Reflector } from '@nestjs/core';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import * as _ from 'lodash';

/**
 * Checks user membership using WooCommerce Memberships
 * https://woocommerce.com/fr/products/woocommerce-memberships/
 * https://woocommerce.com/document/woocommerce-memberships/
 * https://godaddy-wordpress.github.io/woocommerce-memberships-rest-api-docs/
 */

const cacheShortTTL = 2 * 1000; // 2s
const cacheLongTTL = 24 * 60 * 60 * 1000 * 30; // 30 days
// const cacheLongTTL = cacheShortTTL; // while testing

@Injectable()
export class WooMembershipGuard implements CanActivate {
  private readonly logger = new Logger('WooMembershipGuard');

  constructor(
    private readonly reflector: Reflector,
    private readonly service: WooMembershipService,
    @Inject(CACHE_MANAGER) private readonly cacheManager: Cache,
  ) {}

  private async _userHasPlan(
    userId: string,
    planIds: number[],
  ): Promise<{ hasPlan: true; expires: Date | null } | { hasPlan: false }> {
    const userMemberships = await this.service.getUserMemberships(userId);

    const matchingMemberships = this.service.matchingMemberships(
      userMemberships,
      planIds,
    );

    if (!matchingMemberships.length) return { hasPlan: false }; // no membership

    // const latestEndDate = Math.max(
    //   ...matchingMemberships.map((m) => m.end_date_gmt),
    // );

    // console.log(planIds);
    // console.log(userMemberships);
    // console.log(matchingMemberships);

    if (matchingMemberships.some((m) => m.end_date_gmt === null))
      return { hasPlan: true, expires: null }; // lifetime membership

    // const latestEndDateDebug =
    //   _.max(userMemberships.map((m) => m.end_date_gmt)) ?? false;
    // latestEndDateDebug && console.log(new Date(latestEndDateDebug));

    const latestEndDate = _.max(
      matchingMemberships.map(
        (m) => new Date(m.end_date_gmt + 'Z'),

        // m.end_date_gmt === null ? null : new Date(m.end_date_gmt + 'Z'),
      ),
    );

    if (!latestEndDate) {
      this.logger.error(
        `Failed to get membership expiration for userId:${userId}`,
      );
      this.logger.log(userMemberships);

      // return {hasPlan: false};
    }

    // console.log(latestEndDate);
    // latestEndDate && console.log(new Date(latestEndDate));

    return { hasPlan: true, expires: latestEndDate ?? null };
  }

  private async userHasPlan(
    userId: string,
    planIds: number[],
  ): Promise<boolean> {
    const cacheKeyPlan = [...new Set(planIds)].sort().join(','); // remove duplicates + sort
    const cacheKey = `userHasPlan-${userId}-${cacheKeyPlan}`;
    const cache = await this.cacheManager.get<boolean>(cacheKey);

    if (cache === undefined) {
      const userPlan = await this._userHasPlan(userId, planIds);

      // const hasPlan = hasPlanExpire !== false;

      let cacheTTL: number = cacheShortTTL;

      if (userPlan.hasPlan) {
        if (userPlan.expires === null) {
          // lifetime plan
          cacheTTL = cacheLongTTL;
        } else {
          // else until plan expires
          const planExpiresIn = userPlan.expires.getTime() - Date.now();

          if (planExpiresIn > 0) cacheTTL = planExpiresIn;
        }
      }

      await this.cacheManager.set(cacheKey, userPlan.hasPlan, cacheTTL);

      return userPlan.hasPlan;
    }

    return cache;
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const user: ReqUserDto = request.user;

    if (user.isSuperAdmin || user.role === 'ADMIN') {
      return true;
    }

    if (user.provider === 'wp-oauth' && user.providerId) {
      const requiresOneOfPlanIds = this.reflector.getAllAndMerge<number[]>(
        'woo-membership-plan',
        [context.getHandler(), context.getClass()],
      );

      if (!requiresOneOfPlanIds?.length) return true;

      return this.userHasPlan(user.providerId, requiresOneOfPlanIds);
    }

    return false;
  }
}
