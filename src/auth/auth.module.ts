import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from 'src/user/user.module';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { AuthFenceService } from './auth-fence.service';
import { LocalFenceStrategy } from './strategies/local-auth-fence.strategy';
import { AuthTokenModule } from './auth-token/auth-token.module';
import { PasswordService } from './password.service';
import { ConfigModule } from '@nestjs/config';
import { PrismaModule } from 'src/prisma/prisma.module';
import { WpOauthStrategy } from './strategies/wp-oauth.strategy';
import { MembershipService } from './membership/membership.service';
import { WooMembershipService } from './woo-membership/woo-membership.service';

@Module({
  imports: [
    ConfigModule,
    PrismaModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {
        // expiresIn: process.env.JWT_EXPIRES_IN,
        algorithm: 'HS384',
      },
      verifyOptions: {
        algorithms: ['HS384'],
      },
    }),
    UserModule,
    AuthTokenModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    LocalStrategy,
    JwtStrategy,
    WpOauthStrategy,
    LocalFenceStrategy,
    AuthFenceService,
    PasswordService,
    // MembershipService,
    WooMembershipService,
  ],
  exports: [
    AuthService,
    // MembershipService,
    WooMembershipService,
  ],
})
export class AuthModule {}
