import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ReqUserDto } from '../dto/req-user.dto';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.getAllAndOverride<string[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!roles || !roles.length) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const user: ReqUserDto = request.user;

    if (user.isSuperAdmin) {
      return true;
    }

    if (user.role === 'ADMIN') {
      return true;
    }

    return roles.includes(user.role);

    // const userRoles = user.role.map((r) => r.id);

    // return roles.some(
    //   (r) => userRoles.some((ur) => ur === r || ur === 'ADMIN'), // ADMIN gives all roles
    // );
  }
}
