import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class WpOauthGuard extends AuthGuard('wp-oauth') {}
