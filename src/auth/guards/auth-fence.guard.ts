import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthFenceService } from '../auth-fence.service';
import { Reflector } from '@nestjs/core';
import { AuthFenceRules } from '../decorators/auth-fence-rules.decorator';

/**
 * Block access from IP that have abused the login endpoint
 */

@Injectable()
export class AuthFenceGuard implements CanActivate {
  constructor(
    private readonly authFenceService: AuthFenceService,
    private readonly reflector: Reflector,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const rules = this.reflector.get(AuthFenceRules, context.getHandler());
    const request = context.switchToHttp().getRequest();

    await this.authFenceService.authFenceThrow(request, rules);

    return true;
  }
}
