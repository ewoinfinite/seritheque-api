import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

/**
 * LocalAuth strategy with custom name 'local-auth-fence'
 * that will associate with LocalAuthFenceStrategy
 * Basically it logs failed login attempts to AuthFenceLog table
 */

@Injectable()
export class LocalAuthFenceGuard extends AuthGuard('local-auth-fence') {}
