import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotAcceptableException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthToken, Prisma, User } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { AuthLoginResponseDto } from './dto/auth-login-response.dto';
import { ReqUserDto } from './dto/req-user.dto';
import { AuthTokenService } from './auth-token/auth-token.service';
import { PasswordService } from './password.service';
import { AuthLogoutResponseDto } from './dto/auth-logout-response.dto';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
    private readonly authTokenService: AuthTokenService,
    private readonly passwordService: PasswordService,
  ) {}

  // async register(userDto: CreateUserDto): Promise<Omit<User, 'password'>> {
  //   return this.userService.create(userDto);
  // }

  async login(user: User, userAgent: string): Promise<string> {
    const authToken = await this.authTokenService.create(user, userAgent);

    return this.signToken(authToken);
  }

  async logout(user: ReqUserDto): Promise<AuthLogoutResponseDto> {
    try {
      const authTokenDeleted = await this.authTokenService.deleteOne(
        user.authToken.id,
        user.id,
      );

      return <AuthLogoutResponseDto>{
        success: true,
        message: `Successfully logged out`,
      };
    } catch (error) {
      this.logger.error(error);

      throw new InternalServerErrorException(error.message);
    }
  }

  async validateUser(login: string, password: string): Promise<User> {
    let user: User;

    try {
      user = await this.userService.findByLogin(login);
    } catch (error) {
      throw new NotFoundException(
        `There is no active user with login: ${login}`,
      );
    }

    if (
      !(
        user.password &&
        (await this.passwordService.validatePassword(password, user.password))
      )
    ) {
      throw new BadRequestException(
        `Wrong password for user with login: ${login}`,
      );
    }

    return user;
  }

  async verifyPayload(payload: JwtPayload): Promise<ReqUserDto> {
    // payload.sub is an AuthToken.id
    const authTokenId = payload.sub;

    try {
      const result = await this.prisma.user.findFirstOrThrow({
        /**
         * Select only relevant data to have in req.user (matching ReqUserDto)
         */
        select: {
          id: true,
          provider: true,
          providerId: true,
          name: true,
          email: true,
          role: true,
          isSuperAdmin: true,
          langId: true,
          lang: true,
          authTokens: {
            select: { id: true, createdAt: true, expiresAt: true },
            where: {
              id: authTokenId,
            },
          },
        },

        /**
         * User is activated and not expired
         * AuthToken hasn't expired
         */
        where: {
          activatedAt: { not: null },
          OR: [{ expiresAt: null }, { expiresAt: { gt: new Date() } }],
          authTokens: {
            some: {
              id: authTokenId,
              OR: [{ expiresAt: null }, { expiresAt: { gt: new Date() } }],
            },
          },
        },
      });

      const { authTokens, ...user } = result;

      return {
        authToken: authTokens[0],
        ...user,
      };
    } catch (error) {
      if (
        error instanceof Prisma.PrismaClientKnownRequestError &&
        error.code === 'P2025'
      ) {
        throw new UnauthorizedException(
          `There is no active user matching authToken (${payload.sub})`,
        );
      } else {
        this.logger.error(error);
        throw new InternalServerErrorException(error.message);
      }
    }
  }

  signToken(authToken: AuthToken): string {
    const payload = {
      sub: authToken.id,
    };

    return this.jwtService.sign(payload);
  }

  /**
   * Create a user in local db to match user provided by oAuth
   * @param userCreate
   * @param userAgent
   * @returns
   */
  async signIn(
    userCreate: Prisma.UserCreateInput,
    userAgent: string,
  ): Promise<string> {
    const { provider, providerId } = userCreate;

    if (!(provider && providerId))
      throw new NotAcceptableException('Missing provider/providerId');

    const userExists = await this.prisma.user.findUnique({
      where: { provider_providerId: { provider, providerId } },
    });

    let user: User;

    if (userExists) {
      if (
        userExists.email !== userCreate.email ||
        userExists.name !== userCreate.name ||
        userExists.role !== userExists.role
      ) {
        user = await this.prisma.user.update({
          where: { id: userExists.id },
          data: userCreate,
        });
      } else {
        user = userExists;
      }
    } else {
      user = await this.prisma.user.create({ data: userCreate });
    }

    const authToken = await this.authTokenService.create(user, userAgent);

    return this.signToken(authToken);
  }
}
