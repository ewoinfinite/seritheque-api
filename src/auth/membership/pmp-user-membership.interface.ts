export interface PmpUserMembership {
  ID: string; // Int
  id: string; // Int
  subscription_id: string; // Int
  name: string; // Ex: "Séries Numériques Plus",
  description: string; // Ex: "Profitez d'un accès complet et illimité à votre moteur de recherche de séquences numériques avancées pendant une année entière. Le plan <strong>Série Numérique Plus</strong> est conçu spécialement pour les experts cherchant à approfondir leurs connaissances et à maximiser leur efficacité dans leur domaine.",
  confirmation: string; // Ex: "Votre abonnement <strong>Série Numérique Plus </strong>est activé !",
  expiration_number: string; // Ex: "0",
  expiration_period: string; // Ex: "0",
  initial_payment: number; // Ex: 0
  billing_amount: number; // Ex: 0,
  cycle_number: string; // Ex: "0",
  cycle_period: string; // Ex: "month",
  billing_limit: string; // Ex: "0",
  trial_amount: number; // Ex: 0,
  trial_limit: string; // Ex: "0",
  code_id: string; // Ex: "0",
  startdate: string; // Ex: "1718494335",
  enddate: string | null;
}
