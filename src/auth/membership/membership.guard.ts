import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
} from '@nestjs/common';
import { MembershipService } from './membership.service';
import { Reflector } from '@nestjs/core';
import { PmpUserMembership } from './pmp-user-membership.interface';
import { ReqUserDto } from '../dto/req-user.dto';

/**
 * Membership Guard using PayedMembershipPro plugin
 */

@Injectable()
export class MembershipGuard implements CanActivate {
  private readonly logger = new Logger('MembershipGuard');

  constructor(
    private readonly reflector: Reflector,
    private readonly membershipService: MembershipService,
  ) {}

  async getUserMemberships(userId: string): Promise<PmpUserMembership[]> {
    return this.membershipService.getUserMemberships(userId);
  }

  matchesMemberships(
    membershipIds: string[],
    userMemberships: PmpUserMembership[],
  ): boolean {
    return membershipIds.some((membershipId) =>
      userMemberships.some(
        (userMembership) => userMembership.id === membershipId,
      ),
    );
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const membershipIds = this.reflector.getAllAndMerge<string[]>(
      'memberships',
      [context.getHandler(), context.getClass()],
    );
    // console.log(membershipIds);
    if (!membershipIds) return true;

    const request = context.switchToHttp().getRequest();
    const user: ReqUserDto = request.user;

    if (user.isSuperAdmin || user.role === 'ADMIN') {
      return true;
    }

    if (user.provider === 'wp-oauth' && user.providerId) {
      const userMemberships = await this.getUserMemberships(user.providerId);
      const match = this.matchesMemberships(membershipIds, userMemberships);
      // console.log(match);
      return match;
    }

    return false;
  }
}
