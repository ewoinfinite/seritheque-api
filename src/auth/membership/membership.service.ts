import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PmpUserMembership } from './pmp-user-membership.interface';
import axios from 'axios';

@Injectable()
export class MembershipService {
  constructor(
    // private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {}

  async getUserMemberships(userId: string): Promise<PmpUserMembership[]> {
    const url = this.configService.getOrThrow(
      'WP_PMP_GET_USER_MEMBERSHIPS_URL',
    );
    const token = this.configService.getOrThrow('WP_PMP_JWT_TOKEN');

    return axios
      .get<
        PmpUserMembership[]
      >(`${url}?user_id=${userId}`, { headers: { Authorization: `Bearer ${token}` } })
      .then((response) => response.data);
  }
}
