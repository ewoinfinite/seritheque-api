import { SetMetadata } from '@nestjs/common';

export const Memberships = (...args: string[]) =>
  SetMetadata('memberships', args);
