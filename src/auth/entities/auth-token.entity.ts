import { ApiProperty, OmitType } from '@nestjs/swagger';
import { AuthToken } from '@prisma/client';
import { IsUUID } from 'class-validator';

export class AuthTokenEntity implements AuthToken {
  @ApiProperty({
    type: String,
    format: 'uuid',
  })
  @IsUUID(4)
  id: string;

  @ApiProperty({
    type: String,
    format: 'uuid',
  })
  @IsUUID(4)
  userId: string;

  @ApiProperty({
    type: Date,
  })
  createdAt: Date;

  @ApiProperty({
    type: Date,
    nullable: true,
  })
  expiresAt: Date | null;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  userAgent: string;
}
