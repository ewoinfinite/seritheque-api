import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { User } from '@prisma/client';
import { Strategy } from 'passport-local';
import express from 'express';
import { AuthFenceService } from '../auth-fence.service';

/**
 * Log failed login attempts to AuthFenceLog table
 */

@Injectable()
export class LocalFenceStrategy extends PassportStrategy(
  Strategy,
  'local-auth-fence',
) {
  constructor(private readonly authFenceService: AuthFenceService) {
    super({
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true,
      session: false,
    });
  }

  validate(
    req: express.Request,
    username: string,
    password: string,
  ): Promise<User> {
    return this.authFenceService.validateUserWithRequest(
      req,
      username,
      password,
    );
  }
}
