import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import axios from 'axios';
import { Strategy, VerifyCallback } from 'passport-oauth2';

@Injectable()
export class WpOauthStrategy extends PassportStrategy(Strategy, 'wp-oauth') {
  private readonly logger = new Logger(this.name);

  constructor(private readonly configService: ConfigService) {
    super({
      authorizationURL: configService.get('WP_OAUTH_AUTHORIZATION_URL'),
      tokenURL: configService.get('WP_OAUTH_TOKEN_URL'),
      clientID: configService.get('WP_OAUTH_CLIENT_ID'),
      clientSecret: configService.get('WP_OAUTH_CLIENT_SECRET'),
      callbackURL: configService.get('WP_OAUTH_CALLBACK_URL'),
      // scope: ['openid', 'profile', 'email'],
      scope: ['profile', 'email'],
      // state: 'test-state',
    });
  }

  validate(
    // ...args: any
    accessToken: string,
    refreshToken: string,
    // results: any,
    profile: any,
    done: VerifyCallback,
  ): void {
    // console.log(args);
    // const { id, name, email, photos } = profile;

    // console.log('toto accessToken', accessToken);
    // console.log('toto refreshToken', refreshToken);
    // console.log('toto results', results);
    // console.log('toto profile', profile);

    // const user = {
    //   provider: 'wp-oauth',
    //   providerId: id,
    //   email: email,
    //   // name: `${name.givenName} ${name.familyName}`,
    //   // picture: photos[0].value,
    // };

    done(null, profile);
  }

  userProfile(accessToken: string, done: any): void {
    // console.log('tata accessToken', accessToken);

    const url = this.configService.get('WP_OAUTH_PROFILE_URL');
    if (!url) {
      let error = `Missing required WP_OAUTH_PROFILE_URL`;

      this.logger.error(error);

      throw new InternalServerErrorException(error);
    }

    axios
      .get(url, {
        headers: { Authorization: `Bearer ${accessToken}` },
        maxRedirects: 0,
      })
      .catch((error) => {
        this.logger.error('WP oAuth: Failed to fetch user profile');
        this.logger.error(error);

        done(
          new InternalServerErrorException(
            'WP oAuth: Failed to fetch user profile',
          ),
        );
      })
      .then((response) => done(null, response?.data ?? null));
  }

  // parseErrorResponse(body: any, status: number): Error | null {
  //   console.log(body);
  //   console.log(status);

  //   return null;
  // }
}
