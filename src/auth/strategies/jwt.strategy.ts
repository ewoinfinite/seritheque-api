import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, JwtFromRequestFunction, Strategy } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { AuthToken, User } from '@prisma/client';

// const extractJwtFromCookie: JwtFromRequestFunction = (request) => {
//   return request.signedCookies['token']!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
// };

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private readonly authService: AuthService) {
    super({
      // jwtFromRequest: ExtractJwt.fromExtractors([
      //   // extractJwtFromCookie,
      //   ExtractJwt.fromAuthHeaderAsBearerToken(),
      // ]),

      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: process.env.JWT_SECRET,
      passReqToCallback: false,
    });
  }

  validate(payload: JwtPayload): Promise<Partial<User>> {
    return this.authService.verifyPayload(payload);
  }

  // async validate(payload: JwtUserDto): Promise<ReqUserDto> {
  //   const reqUser: ReqUserDto = {
  //     id: payload.sub,
  //     name: payload.username,
  //   };

  //   return reqUser;
  // }
}
