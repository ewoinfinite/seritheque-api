import {
  ForbiddenException,
  Injectable,
  Logger,
  NotAcceptableException,
} from '@nestjs/common';
import express from 'express';
import { PrismaService } from 'src/prisma/prisma.service';
import { AuthService } from './auth.service';
import { User } from '@prisma/client';

export type AuthFenceRule = { ttl: number; limit: number };

@Injectable()
export class AuthFenceService {
  private readonly logger = new Logger(AuthFenceService.name);

  constructor(
    private readonly prisma: PrismaService,
    private readonly authService: AuthService,
  ) {}

  /**
   * Called from LocalFenceStrategy to log failed login attempts
   * Passport LocalAuthFenceGuard passes us the original request so we can extract client IP
   * @param req
   * @param login
   * @param password
   * @returns {User}
   */
  async validateUserWithRequest(
    req: express.Request,
    login: string,
    password: string,
  ): Promise<User> {
    return this.authService.validateUser(login, password).catch((error) => {
      const ip = req.ips.length ? req.ips[0] : req.ip;

      if (!ip) throw new NotAcceptableException();

      this.authFenceLog(ip, login);

      throw error;
    });
  }

  async authFenceLog(ip: string, login: string) {
    // if we don't await here log entry isn't created
    // sounds like soon coming throw aborts any running async thread still active
    // adding .then() seems to fix the problem
    this.prisma.authFenceLog
      .create({
        data: {
          ip,
          login,
        },
      })
      .then(); // hack so async call doesn't get lost without writing to table

    this.logger.warn(`Failed login (${login}) from ip (${ip})`);
  }

  /**
   * If request comes from abuser IP found in AuthFenceLog table it will throw an ForbiddenException
   * @param req express.Request
   * @param rules default [{ ttl: 600, limit: 5 }] will allow maximum of 5 requests in past 10 minutes
   */

  async authFenceThrow(
    req: express.Request,
    rules: AuthFenceRule[] = [{ ttl: 600, limit: 5 }], // default 5 in 10min
  ): Promise<void> {
    const ip = req.ips.length ? req.ips[0] : req.ip;

    await Promise.all(
      rules.map(async (rule) => {
        const count = await this.prisma.authFenceLog.count({
          where: {
            ip,
            createdAt: { gt: new Date(Date.now() - rule.ttl * 1000) },
          },
        });

        if (count >= rule.limit) {
          this.logger.warn(`Blocked ip (${ip})`);
          throw new ForbiddenException(`Too many failed attempts`);
        }
      }),
    );
  }
}
