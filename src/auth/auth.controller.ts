import {
  Controller,
  Post,
  UseGuards,
  HttpStatus,
  Get,
  Header,
  UnauthorizedException,
  Query,
  Req,
  Res,
  Logger,
  InternalServerErrorException,
  NotAcceptableException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import {
  ApiResponse,
  ApiBody,
  ApiTags,
  ApiOkResponse,
  ApiOperation,
  ApiBearerAuth,
  ApiOAuth2,
} from '@nestjs/swagger';
import { AuthLoginDto } from './dto/auth-login.dto';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { AuthFenceGuard } from './guards/auth-fence.guard';
import { AuthFenceRules } from './decorators/auth-fence-rules.decorator';
import { LocalAuthFenceGuard } from './guards/local-auth-fence.guard';
import { AuthLoginResponseDto } from './dto/auth-login-response.dto';
import { ReqUserDto } from './dto/req-user.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { Prisma, User, UserRoleEnum } from '@prisma/client';
import express, { Request } from 'express';
import { AuthLogoutResponseDto } from './dto/auth-logout-response.dto';
import { WpOauthGuard } from './guards/wp-oauth.guard';
import { Response } from 'express';
import { ConfigService } from '@nestjs/config';

type WpUser = {
  ID: string;
  user_login: string;
  user_nicename: string;
  display_name: string;
  user_email: string;
  user_registered: Date;
  user_status: number;
  user_roles: string[];
};

@Controller('auth')
@ApiTags('Auth')
@ApiOAuth2(['email', 'profile'])
export class AuthController {
  private readonly logger = new Logger('AuthController');

  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {}

  @Post('login')
  @AuthFenceRules([
    { ttl: 600, limit: 5 }, // maximum of 5 failed logins in last 10min
    { ttl: 3600, limit: 10 }, // 10 in last hour
  ])
  // Both are required.
  // AuthFenceGuard will block requests coming from flagged IPs according to @AuthFenceRules
  // LocalAuthFenceGuard is a LocalAuth Passport guard that logs failed logins into AuthFenceLog table
  @UseGuards(AuthFenceGuard, LocalAuthFenceGuard)
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: AuthLoginResponseDto,
    description: `Authorized`,
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: `Unauthorized`,
  })
  @ApiResponse({
    status: HttpStatus.FORBIDDEN,
    description: `Too many failed attempts. Your IP has been blocked.`,
  })
  @ApiBody({ type: AuthLoginDto })
  async login(
    @Req() req: express.Request & { user: User },
  ): Promise<AuthLoginResponseDto> {
    const user: User = req.user;
    const userAgent = req.headers['user-agent'];

    if (!userAgent) throw new UnauthorizedException();

    const token = await this.authService.login(user, userAgent);

    return { token };
  }

  @Post('logout')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: AuthLogoutResponseDto,
    description: `Successfully logged out`,
  })
  async logout(
    @Req() req: express.Request & { user: ReqUserDto },
  ): Promise<AuthLogoutResponseDto> {
    return this.authService.logout(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get('user')
  @Header('Cache-Control', 'no-cache, no-store')
  @ApiOperation({
    summary: `Returns <ReqUserDto>Request.user (Contains AuthToken of the current authentication)`,
  })
  @ApiOkResponse({ type: ReqUserDto })
  async me(@Req() req: Request & { user: ReqUserDto }): Promise<ReqUserDto> {
    return req.user;
  }

  @Get('wp-oauth/callback')
  @UseGuards(WpOauthGuard)
  async wpOauthCallback(
    @Req() req: Request & { user: WpUser },
    @Res() res: Response,
  ) {
    // console.log('wpOauthCallback user', req.user);

    const userAgent = req.headers['user-agent'];
    if (!userAgent) throw new NotAcceptableException();

    if (!req.user?.user_roles) {
      throw new InternalServerErrorException(
        'WP oAuth: failed to fetch a proper user',
      );
    }

    let role: UserRoleEnum = req.user.user_roles.includes('administrator')
      ? UserRoleEnum.ADMIN
      : UserRoleEnum.USER;

    const user: Prisma.UserCreateInput = {
      provider: 'wp-oauth',
      providerId: req.user.ID,
      name: req.user.user_login,
      email: req.user.user_email,
      role,
      activatedAt: new Date(),
    };

    const authToken = await this.authService.signIn(user, userAgent);

    // res.cookie('access_token', token, {
    //   maxAge: 2592000000,
    //   sameSite: true,
    //   secure: false,
    // });

    // return res.status(HttpStatus.OK);

    // return { token };

    const redirectUrl = this.configService.get('WP_OAUTH_REDIRECT_URL') ?? '/';

    // if (!redirectUrl) {
    //   let error = `Missing required WP_OAUTH_REDIRECT_URL`;
    //   this.logger.error(error);
    //   throw new InternalServerErrorException(error);
    // }

    res.redirect(`${redirectUrl}?auth_token=${authToken}`);
  }

  @Get('wp-oauth')
  @UseGuards(WpOauthGuard)
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  async wpOauth() {
    // return { status: 'ok' };
  }
}
