import { Reflector } from '@nestjs/core';
import { AuthFenceRule } from '../auth-fence.service';

export const AuthFenceRules = Reflector.createDecorator<AuthFenceRule[]>();
