import { Injectable } from '@nestjs/common';
import { CreateLangDto } from './dto/create-lang.dto';
import { UpdateLangDto } from './dto/update-lang.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { UtilsService } from 'src/utils/utils.service';
import { Lang, Prisma } from '@prisma/client';
import { LangEntity } from './entities/lang.entity';

@Injectable()
export class LangService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly utils: UtilsService,
  ) {}

  create(createLangDto: CreateLangDto) {
    return 'This action adds a new lang';
  }

  async findAll(): Promise<LangEntity[]> {
    return this.prisma.lang.findMany();

    // return `This action returns all lang`;
  }

  findOne(id: number) {
    return `This action returns a #${id} lang`;
  }

  update(id: number, updateLangDto: UpdateLangDto) {
    return `This action updates a #${id} lang`;
  }

  remove(id: number) {
    return `This action removes a #${id} lang`;
  }
}
