import { ApiProperty } from '@nestjs/swagger';
import { Lang } from '@prisma/client';

export class LangEntity implements Lang {
  @ApiProperty({
    type: String,
    example: 'fr',
  })
  id: string;

  @ApiProperty({
    type: String,
    example: 'French',
  })
  name: string;

  @ApiProperty({
    type: String,
    example: 'Français',
  })
  localName: string;
}
