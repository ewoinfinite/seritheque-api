import { Module } from '@nestjs/common';
import { LangService } from './lang.service';
import { LangController } from './lang.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UtilsService } from 'src/utils/utils.service';

@Module({
  imports: [PrismaModule],
  controllers: [LangController],
  providers: [LangService, UtilsService],
})
export class LangModule {}
