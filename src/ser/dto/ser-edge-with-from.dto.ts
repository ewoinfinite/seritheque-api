import { ApiProperty } from '@nestjs/swagger';
import { SerEdgeEntity } from '../entities/ser-edge.entity';
import { SerWithContentsDto } from './ser-with-contents.dto';

export class SerEdgeWithFromDto extends SerEdgeEntity {
  @ApiProperty({
    type: SerWithContentsDto,
  })
  from: SerWithContentsDto;
}
