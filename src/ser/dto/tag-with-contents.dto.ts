import { ApiProperty } from '@nestjs/swagger';
import { TagEntity } from '../entities/tag.entity';
import { TagContentEntity } from '../entities/tag-content.entity';

export class TagWithContentsDto extends TagEntity {
  @ApiProperty({
    type: TagContentEntity,
    isArray: true,
  })
  contents: TagContentEntity[];
}
