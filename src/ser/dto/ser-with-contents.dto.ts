import { ApiProperty } from '@nestjs/swagger';
import { SerEntity } from '../entities/ser.entity';
import { SerContentEntity } from '../entities/ser-content.entity';

export class SerWithContentsDto extends SerEntity {
  @ApiProperty({
    type: SerContentEntity,
    isArray: true,
  })
  contents: SerContentEntity[];
}
