import { PaginatedResult } from 'src/prisma/paginator/prisma-paginator';
import { SerFindManyItem, SerFindManyItemDto } from './ser-find-many-item.dto';
import { PaginatedResponseDto } from 'src/prisma/paginator/paginated-response.dto';
import { ApiProperty } from '@nestjs/swagger';

export class SerFindManyPaginatedDto
  extends PaginatedResponseDto
  implements PaginatedResult<SerFindManyItem>
{
  @ApiProperty({
    type: SerFindManyItemDto,
    isArray: true,
  })
  data: SerFindManyItem[];
}
