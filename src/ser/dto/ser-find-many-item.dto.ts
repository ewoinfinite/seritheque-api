import { ApiProperty } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { SourceWithContentsDto } from 'src/source/dto/source-with-contents.dto';

export const serFindManyItem = Prisma.validator<Prisma.SerDefaultArgs>()({
  select: {
    id: true,
    name: true,
    nameNoSpace: true,
    sourceId: true,
    contents: {
      // where: { langId },
      select: {
        id: true,
        title: true,
        excerpt: true,
      },
    },
  },
});

export type SerFindManyItem = Prisma.SerGetPayload<typeof serFindManyItem>;

export class SerFindManyItemContentDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  title: string;

  @ApiProperty()
  excerpt: string;
}

export class SerFindManyItemDto implements SerFindManyItem {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  nameNoSpace: string;

  @ApiProperty({
    type: SerFindManyItemContentDto,
    isArray: true,
  })
  contents: SerFindManyItemContentDto[];

  @ApiProperty({
    type: Number,
    nullable: true,
  })
  sourceId: number | null;
}
