import { ApiProperty } from '@nestjs/swagger';
import { SerWithContentsDto } from './ser-with-contents.dto';
import { TagWithContentsDto } from './tag-with-contents.dto';
import { SerEdgeWithToDto } from './ser-edge-with-to.dto';
import { SerEdgeWithFromDto } from './ser-edge-with-from.dto';
import { SimpleCategoryDto } from 'src/category/dto/simple-category.dto';
import { SerDetail } from './ser-detail.dto';
import { KeywordEntity } from '../entities/keyword.entity';
import { SourceWithContentsDto } from 'src/source/dto/source-with-contents.dto';

export type SimpleSerDetail = Omit<SerDetail, 'categories'> & {
  simpleCategories: SimpleCategoryDto[];
};

export class SimpleSerDetailDto
  extends SerWithContentsDto
  implements SimpleSerDetail
{
  @ApiProperty({
    type: SimpleCategoryDto,
    isArray: true,
  })
  simpleCategories: SimpleCategoryDto[];

  @ApiProperty({
    type: TagWithContentsDto,
    isArray: true,
  })
  tags: TagWithContentsDto[];

  @ApiProperty({
    type: KeywordEntity,
    isArray: true,
  })
  keywords: KeywordEntity[];

  @ApiProperty({
    type: SerEdgeWithToDto,
    isArray: true,
  })
  edges: SerEdgeWithToDto[];

  @ApiProperty({
    type: SerEdgeWithFromDto,
    isArray: true,
  })
  revEdges: SerEdgeWithFromDto[];

  @ApiProperty({
    type: SourceWithContentsDto,
    nullable: true,
  })
  source: SourceWithContentsDto | null;

  // @ApiProperty({
  //   type: SerToSourceWithSourceDto,
  //   isArray: true,
  // })
  // serToSources: SerToSourceWithSourceDto[];
}
