import { PartialType } from '@nestjs/mapped-types';
import { CreateSerDto } from './create-ser.dto';

export class UpdateSerDto extends PartialType(CreateSerDto) {}
