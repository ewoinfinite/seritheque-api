import { Prisma } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';
import { SerWithContentsDto } from './ser-with-contents.dto';
import { TagWithContentsDto } from './tag-with-contents.dto';
import { SerEdgeWithToDto } from './ser-edge-with-to.dto';
import { SerEdgeWithFromDto } from './ser-edge-with-from.dto';
import { SimpleCategoryDto } from 'src/category/dto/simple-category.dto';
import { CategoryWithContentsDto } from 'src/category/dto/category-with-contents.dto';
import { KeywordEntity } from '../entities/keyword.entity';
import { SourceWithContentsDto } from 'src/source/dto/source-with-contents.dto';

export const serDetail = Prisma.validator<Prisma.SerDefaultArgs>()({
  select: {
    id: true,
    name: true,
    createdAt: true,
    updatedAt: true,

    contents: true,
    categories: { include: { contents: true } },
    tags: { include: { contents: true } },
    keywords: true,
    edges: { include: { to: { include: { contents: true } } } },
    revEdges: { include: { from: { include: { contents: true } } } },
    // serToSources: { include: { source: { include: { contents: true } } } },
    source: { include: { contents: true } },
  },
});

export type SerDetail = Prisma.SerGetPayload<typeof serDetail>;

export class SerDetailDto extends SerWithContentsDto implements SerDetail {
  @ApiProperty({
    type: CategoryWithContentsDto,
    isArray: true,
  })
  categories: CategoryWithContentsDto[];

  @ApiProperty({
    type: TagWithContentsDto,
    isArray: true,
  })
  tags: TagWithContentsDto[];

  @ApiProperty({
    type: KeywordEntity,
    isArray: true,
  })
  keywords: KeywordEntity[];

  @ApiProperty({
    type: SerEdgeWithToDto,
    isArray: true,
  })
  edges: SerEdgeWithToDto[];

  @ApiProperty({
    type: SerEdgeWithFromDto,
    isArray: true,
  })
  revEdges: SerEdgeWithFromDto[];

  @ApiProperty({
    type: SourceWithContentsDto,
    nullable: true,
  })
  source: SourceWithContentsDto | null;

  // @ApiProperty({
  //   type: SerToSourceWithSourceDto,
  //   isArray: true,
  // })
  // serToSources: SerToSourceWithSourceDto[];
}
