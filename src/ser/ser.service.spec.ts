import { Test, TestingModule } from '@nestjs/testing';
import { SerService } from './ser.service';
import { PrismaService } from 'src/prisma/prisma.service';

describe('SerService', () => {
  let service: SerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SerService, PrismaService],
    }).compile();

    service = module.get<SerService>(SerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
