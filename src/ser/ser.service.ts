import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotAcceptableException,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import {
  PaginatedResult,
  paginator,
} from 'src/prisma/paginator/prisma-paginator';
import { paginatorPerPageDefault } from 'src/shared/constants';
import { Prisma, Ser } from '@prisma/client';
import { SerFindManyQuery } from './queries/ser-find-many.query';
import { unAccent } from 'src/utils/utils';
import { SerDetailDto } from './dto/ser-detail.dto';
import { UtilsService } from 'src/utils/utils.service';
import { SimpleSerDetailDto } from './dto/simple-ser-detail.dto';
import { SerFindManyItem } from './dto/ser-find-many-item.dto';

// const serDetailSelect = (langId?: string): Prisma.SerSelect => {
//   const result: Prisma.SerSelect = {
//     id: true,
//     name: true,
//     nameNoSpace: true,
//     sourceId: true,
//     sourceReference: true,
//     createdAt: true,
//     updatedAt: true,
//     contents: { where: { langId } },
//     categories: { include: { contents: { where: { langId } } } },
//     tags: { include: { contents: { where: { langId } } } },
//     keywords: { where: { langId } },
//     edges: {
//       include: { to: { include: { contents: { where: { langId } } } } },
//     },
//     revEdges: {
//       include: { from: { include: { contents: { where: { langId } } } } },
//     },
//     source: { include: { contents: { where: { langId } } } },
//   };

//   return result;
// };

@Injectable()
export class SerService {
  private readonly logger = new Logger(SerService.name);

  constructor(
    private readonly prisma: PrismaService,
    private readonly utils: UtilsService,
  ) {}

  async findOneDetail(id: number, langId?: string): Promise<SerDetailDto> {
    langId = langId ?? undefined;

    return await this.prisma.ser
      .findUniqueOrThrow({
        where: { id },
        select: {
          id: true,
          name: true,
          nameNoSpace: true,
          sourceId: true,
          sourceReference: true,
          createdAt: true,
          updatedAt: true,
          order: true,
          contents: { where: { langId } },
          categories: { include: { contents: { where: { langId } } } },
          tags: { include: { contents: { where: { langId } } } },
          keywords: { where: { langId } },
          edges: {
            include: { to: { include: { contents: { where: { langId } } } } },
            where: { to: { contents: { some: { langId } } } },
          },
          revEdges: {
            include: { from: { include: { contents: { where: { langId } } } } },
            where: { from: { contents: { some: { langId } } } },
          },
          source: { include: { contents: { where: { langId } } } },
        },
      })
      .catch((error) => {
        if (
          error instanceof Prisma.PrismaClientKnownRequestError &&
          error.code === 'P2025'
        ) {
          throw new NotFoundException();
        } else {
          this.logger.error(error);
          throw new InternalServerErrorException(error.message);
        }
      });

    // return {
    //   ...result,
    //   categories: result.categories.map(
    //     this.utils.categoryWithContentsToCategorySimple,
    //   ),
    // };
  }

  async findOneDetailSimple(
    id: number,
    langId: string,
  ): Promise<SimpleSerDetailDto> {
    return this.findOneDetail(id, langId).then(
      this.utils.serDetailToSimpleSerDetail,
    );
  }

  async findByNameNoSpaceDetail(
    nameNoSpace: string,
    langId?: string,
  ): Promise<SerDetailDto[]> {
    langId = langId ?? undefined;

    const result = await this.prisma.ser
      .findMany({
        where: { nameNoSpace },
        select: {
          id: true,
          name: true,
          nameNoSpace: true,
          sourceId: true,
          sourceReference: true,
          createdAt: true,
          updatedAt: true,
          order: true,
          contents: { where: { langId } },
          categories: {
            include: { contents: { where: { langId } } },
            where: { contents: { some: { langId } } },
          },
          tags: { include: { contents: { where: { langId } } } },
          keywords: { where: { langId } },
          edges: {
            include: { to: { include: { contents: { where: { langId } } } } },
            where: { to: { contents: { some: { langId } } } },
          },
          revEdges: {
            include: { from: { include: { contents: { where: { langId } } } } },
            where: { from: { contents: { some: { langId } } } },
          },
          source: { include: { contents: { where: { langId } } } },
        },
      })
      .catch((error) => {
        this.logger.error(error);
        throw new InternalServerErrorException(error.message);
      });

    if (!result.length) {
      throw new NotFoundException();
    }

    return result;
  }

  async findByNameNoSpaceDetailSimple(
    nameNoSpace: string,
    langId: string,
  ): Promise<SimpleSerDetailDto[]> {
    return this.findByNameNoSpaceDetail(nameNoSpace, langId).then((result) => {
      return result.map(this.utils.serDetailToSimpleSerDetail);
    });
  }

  async serFindManyPaginated(
    query: SerFindManyQuery,
  ): Promise<PaginatedResult<SerFindManyItem>> {
    const timerStart = Date.now();

    const { page, perPage, ...queryWithoutPage } = query;

    const paginate = paginator({ perPage: paginatorPerPageDefault });
    const response = await paginate<SerFindManyItem, Prisma.SerFindManyArgs>(
      this.prisma.ser,
      this.serFindManyArgs(queryWithoutPage),
      {
        page,
        perPage,
      },
    ).catch((error) => {
      this.logger.log(error);
      throw new NotAcceptableException(error.message);
    });
    const timerEnd = Date.now();
    this.logger.debug(`serFindMany in ${timerEnd - timerStart}ms`);
    return response;
  }

  private serFindManyArgs(query: SerFindManyQuery): Prisma.SerFindManyArgs {
    const langId = query.langId ?? undefined;
    const titleOnly = query.titleOnly ?? false;
    const sourceIds = query.sourceIds ?? [];

    const args: Prisma.SerFindManyArgs = {
      select: {
        id: true,
        nameNoSpace: true,
        name: true,
        contents: {
          where: { langId },
          select: {
            id: true,
            title: true,
            excerpt: true,
          },
        },
      },
      // where: {
      //   OR: [],
      //   AND: [],
      // },
    };

    args.where = args.where || { contents: { some: { langId } } };

    const q = query.q?.replace(/\s+/, ' ').trim() || undefined;
    if (q) {
      args.where.OR = [];

      // let qNumArr = /(\d+(?:\p{P}|\s)*)+/gu.exec(q);

      let qNumArr = /((?:^|\s)[\d\s]+(?:$|\s)*)+/u.exec(q);

      let qNum = qNumArr ? qNumArr[0].replace(/[^\d]+/g, '') : null;
      let qTxt =
        unAccent(
          q
            // .replace(/[0-9]+/g, '')
            .replace(/\b[ld]['’]/gi, '') // remove  l' d'
            // .replace(/['’"`]/g, ' ') // retire les guillemets
            // .replace(/\p{P}+/u, ' ') // retire toute ponctuation
            .replace(/\s+/g, ' ')
            .trim(),
        ).toLocaleLowerCase() || '';

      // remove le la les
      qTxt = qTxt
        .split(' ')
        .filter((w) => !['le', 'la', 'les'].includes(q))
        .join(' ')
        .replace(/\s+/g, ' ');

      const qTxtNoAccent = qTxt.replace(/\p{P}+/u, ' ');

      let qTxtLike = qTxt.replace(/\s/g, '%');
      let qTxtLikeNoAccent = qTxtNoAccent.replace(/\s/g, '%');

      if (qNum) {
        args.where.OR.push({ nameNoSpace: { startsWith: qNum } });
      }

      if (qTxt) {
        let or1: Prisma.SerContentWhereInput[] = [
          { noAccentTitle: { contains: qTxtLikeNoAccent } },
        ];

        if (!titleOnly) {
          or1.push({ content: { contains: qTxtLike, mode: 'insensitive' } }); // this might be hard on db

          args.where.OR.push({
            categories: {
              some: {
                contents: {
                  some: {
                    langId,
                    name: {
                      contains: qTxtLike,
                      mode: 'insensitive',
                    },
                  },
                },
              },
            },
          });
        }

        args.where.OR.push({
          keywords: {
            some: {
              OR: [
                {
                  langId,
                  noAccentWord: { contains: qTxtLike },
                },
              ],
            },
          },
        });

        args.where.OR.push({
          contents: {
            some: {
              langId,
              OR: or1,
            },
          },
        });
      }
    }

    if (sourceIds?.length) {
      args.where.AND = [];
      args.where.AND.push({ sourceId: { in: sourceIds } });
    }

    if (query.categoryIds) {
      const categoryIds = (
        Array.isArray(query.categoryIds)
          ? query.categoryIds
          : [query.categoryIds]
      )?.map((n) => +n);

      if (categoryIds?.length) {
        args.where = args.where || {};

        args.where.categories = {
          some: {
            id: { in: categoryIds },
          },
        };
      }
    }

    args.orderBy = args.orderBy || { id: 'asc' };

    // console.log(JSON.stringify(args));

    return args;
  }
}
