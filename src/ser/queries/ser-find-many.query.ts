import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  Matches,
  Max,
  Min,
  MinLength,
} from 'class-validator';
import { PaginateOptionsDto } from 'src/prisma/paginator/paginate-options.dto';

export enum BooleanEnum {
  'true' = '0',
  'false' = '1',
}

export class SerFindManyQuery extends PaginateOptionsDto {
  @ApiProperty({
    type: String,
    required: false,
    example: 'vitamine',
  })
  @IsOptional()
  @IsString()
  q: string;

  @ApiProperty({
    type: String,
    required: false,
    example: 'fr',
  })
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  langId?: string;

  @ApiProperty({
    type: Number,
    isArray: true,
    name: 'categoryIds[]',
    required: false,
  })
  @IsOptional()
  @IsArray()
  @Matches(/^\d+$/, { each: true })
  categoryIds?: number[];

  @ApiProperty({
    enum: BooleanEnum,
    required: false,
  })
  @IsOptional()
  @Transform(({ value }) => !(value == 0 || value == 'false' || value == false))
  @IsBoolean()
  titleOnly?: boolean;

  @ApiProperty({
    type: Number,
    isArray: true,
    required: false,
    name: 'sourceIds[]',
  })
  @IsOptional()
  // @Matches(/^\d+$/, { each: true })
  @Transform(({ value }) => value.map((v: string) => +v))
  sourceIds?: number[];

  @ApiPropertyOptional({
    type: Number,
  })
  @IsOptional()
  @IsInt()
  @Min(0)
  @Max(10)
  page?: number;

  @ApiPropertyOptional({
    type: Number,
    default: 50,
    maximum: 500,
    minimum: 1,
  })
  @IsOptional()
  @IsInt()
  @Max(50)
  @Min(1)
  perPage?: number;
}
