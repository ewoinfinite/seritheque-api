import { Test, TestingModule } from '@nestjs/testing';
import { SerController } from './ser.controller';
import { SerService } from './ser.service';
import { PrismaModule } from 'src/prisma/prisma.module';

describe('SerController', () => {
  let controller: SerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule],
      controllers: [SerController],
      providers: [SerService],
    }).compile();

    controller = module.get<SerController>(SerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
