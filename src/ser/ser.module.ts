import { Module } from '@nestjs/common';
import { SerService } from './ser.service';
import { SerController } from './ser.controller';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UtilsService } from 'src/utils/utils.service';
import { MembershipService } from 'src/auth/membership/membership.service';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [PrismaModule, AuthModule],
  controllers: [SerController],
  providers: [SerService, UtilsService],
})
export class SerModule {}
