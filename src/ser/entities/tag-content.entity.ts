import { ApiProperty } from '@nestjs/swagger';
import { TagContent } from '@prisma/client';

export class TagContentEntity implements TagContent {
  @ApiProperty()
  tagId: number;

  @ApiProperty()
  langId: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  description: string | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
