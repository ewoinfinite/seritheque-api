import { ApiProperty } from '@nestjs/swagger';
import { SerContent } from '@prisma/client';

export class SerContentEntity implements SerContent {
  @ApiProperty()
  id: number;

  @ApiProperty()
  serId: number;

  @ApiProperty()
  langId: string;

  @ApiProperty()
  title: string;

  @ApiProperty()
  content: string;

  @ApiProperty()
  excerpt: string;

  @ApiProperty()
  noAccentTitle: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
