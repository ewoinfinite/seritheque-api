import { ApiProperty } from '@nestjs/swagger';
import { Ser } from '@prisma/client';

export class SerEntity implements Ser {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  nameNoSpace: string;

  @ApiProperty({ type: Number, nullable: true })
  sourceId: number | null;

  @ApiProperty({ type: String, nullable: true })
  sourceReference: string | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty({ nullable: true, type: Number })
  order: number | null;
}
