import { ApiProperty } from '@nestjs/swagger';
import { SerEdge } from '@prisma/client';

export class SerEdgeEntity implements SerEdge {
  @ApiProperty()
  fromId: number;

  @ApiProperty()
  toId: number;

  @ApiProperty()
  order: number | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
