import { ApiProperty } from '@nestjs/swagger';
import { Keyword } from '@prisma/client';

export class KeywordEntity implements Keyword {
  @ApiProperty()
  id: number;

  @ApiProperty()
  langId: string;

  @ApiProperty()
  word: string;

  @ApiProperty()
  noAccentWord: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
