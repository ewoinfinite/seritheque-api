import {
  Controller,
  Get,
  Param,
  Header,
  Query,
  ParseIntPipe,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { SerService } from './ser.service';
import { PaginatedResult } from 'src/prisma/paginator/prisma-paginator';
import { Ser } from '@prisma/client';
import { SerFindManyQuery } from './queries/ser-find-many.query';
import { SerDetailDto } from './dto/ser-detail.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { SimpleSerDetailDto } from './dto/simple-ser-detail.dto';
import { SerFindManyItem } from './dto/ser-find-many-item.dto';
import { SerFindManyPaginatedDto } from './dto/ser-find-many-paginated.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { MembershipGuard } from 'src/auth/membership/membership.guard';
import { Memberships } from 'src/auth/membership/memberships.decorator';
import { WooMembershipGuard } from 'src/auth/woo-membership/woo-membership.guard';
import { WooMembershipPlan } from 'src/auth/woo-membership/woo-membership-plan.decorator';

@ApiTags('Series')
@Controller('ser')
// @Memberships('1', '2', '3', '4', '5')
// @UseGuards(JwtAuthGuard, MembershipGuard)
@WooMembershipPlan(
  28882, // Super Parrain LIVE
  // 23227, // Super Parrain
  23228, // Séries Numériques Cadeau
  23231, // Série Numériques Plus
)
@UseGuards(JwtAuthGuard, WooMembershipGuard)
@ApiBearerAuth()
export class SerController {
  constructor(private readonly serService: SerService) {}

  // @Memberships('1', '2', '3', '4', '5')
  @Get('')
  @Header('Cache-Control', 'no-cache, no-store')
  @ApiOkResponse({ type: SerFindManyPaginatedDto })
  async findMany(
    @Query(new ValidationPipe({ transform: true })) query: SerFindManyQuery,
  ): Promise<SerFindManyPaginatedDto> {
    return this.serService.serFindManyPaginated(query);
  }

  @ApiOkResponse({
    type: SerDetailDto,
  })
  @Get('by-id/:id')
  findOne(@Param('id', ParseIntPipe) id: number): Promise<SerDetailDto> {
    return this.serService.findOneDetail(id);
  }

  // @ApiOkResponse({
  //   type: SerDetailDto,
  // })
  // @Get(':id/:lang')
  // findOneLang(
  //   @Param('id') id: string,
  //   @Param('lang') lang: string,
  // ): Promise<SerDetailDto> {
  //   return this.serService.findOneDetail(id, lang);
  // }

  @ApiOkResponse({
    type: SimpleSerDetailDto,
  })
  @Get('by-id-simple/:id/:langId')
  findOneSimple(
    @Param('id', ParseIntPipe) id: number,
    @Param('langId') langId: string,
  ): Promise<SimpleSerDetailDto> {
    return this.serService.findOneDetailSimple(id, langId);
  }

  // @Get('by-name/:nameNoSpace')
  // findByName(@Param('nameNoSpace') nameNoSpace: string) {}

  @Get('by-name-simple/:nameNoSpace/:langId')
  findByNameSimple(
    @Param('nameNoSpace') nameNoSpace: string,
    @Param('langId') langId: string,
  ): Promise<SimpleSerDetailDto[]> {
    return this.serService.findByNameNoSpaceDetailSimple(nameNoSpace, langId);
  }

  // @Post()
  // create(@Body() createSerDto: CreateSerDto) {
  //   return this.serService.create(createSerDto);
  // }

  // @Get()
  // findAll() {
  //   return this.serService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.serService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateSerDto: UpdateSerDto) {
  //   return this.serService.update(+id, updateSerDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.serService.remove(+id);
  // }
}
