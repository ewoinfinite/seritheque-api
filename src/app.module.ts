import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SerModule } from './ser/ser.module';
import { ConfigModule } from '@nestjs/config';
import { UtilsService } from './utils/utils.service';
import { CategoryModule } from './category/category.module';
import { SourceModule } from './source/source.module';
import { LangModule } from './lang/lang.module';
import { AuthModule } from './auth/auth.module';
import { BookmarkModule } from './bookmark/bookmark.module';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
  imports: [
    ConfigModule.forRoot(),
    CacheModule.register({ isGlobal: true }),
    AuthModule,
    SerModule,
    CategoryModule,
    SourceModule,
    LangModule,
    BookmarkModule,
  ],
  controllers: [AppController],
  providers: [AppService, UtilsService],
})
export class AppModule {}
