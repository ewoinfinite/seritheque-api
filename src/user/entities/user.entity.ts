import { ApiProperty } from '@nestjs/swagger';
import { User, UserRoleEnum } from '@prisma/client';
import { Exclude, Expose, Type } from 'class-transformer';

export class UserEntity implements User {
  id: string;
  provider: string;
  providerId: string | null;
  name: string;
  email: string;
  password: string;
  emailVerifyToken: string | null;
  emailVerified: string | null;
  forgotPasswordToken: string | null;
  firstName: string | null;
  lastName: string | null;
  isSuperAdmin: boolean;
  role: UserRoleEnum;
  activatedAt: Date | null;
  expiresAt: Date | null;
  langId: string | null;

  createdAt: Date;
  updatedAt: Date;
  // constructor(data: Partial<UserEntity>) {
  //   super(data);
  // }
}
