import {
  BadRequestException,
  ConflictException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotAcceptableException,
  NotFoundException,
} from '@nestjs/common';
import { Prisma, User } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { UserUpdatePasswordDto } from './dto/user-update-password.dto';
import { ReqUserDto } from 'src/auth/dto/req-user.dto';
import { AuthTokenService } from 'src/auth/auth-token/auth-token.service';
// import { AdminUserFindManyQueryDto } from 'src/admin/admin-user/dto/admin-user-find-many-query.dto';
import {
  PaginatedResult,
  paginator,
} from 'src/prisma/paginator/prisma-paginator';
import { paginatorPerPageDefault } from 'src/shared/constants';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user-dto';
import { PasswordService } from 'src/auth/password.service';
import {
  UserResponseDto,
  UserResponsePrismaPayload,
  UserResponsePrismaPayloadArgs,
} from './dto/user-response.dto';
import { UserFindManyQueryDto } from './query/user-find-many.query';

@Injectable()
export class UserService {
  readonly logger = new Logger(UserService.name);

  constructor(
    private readonly prisma: PrismaService,
    private readonly authTokenService: AuthTokenService,
    private readonly passwordService: PasswordService,
  ) {}

  async findManyPaginated(
    query: UserFindManyQueryDto,
  ): Promise<PaginatedResult<UserResponsePrismaPayload>> {
    const args: Prisma.UserFindManyArgs & UserResponsePrismaPayloadArgs = {
      include: {
        lang: true,
      },
    };

    const order = query.order ? query.order : 'asc';

    if (query.orderBy) {
      args.orderBy = [];
      switch (query.orderBy) {
        case 'langName':
          args.orderBy.push({ lang: { name: order } });
          break;

        default:
          args.orderBy.push({ [query.orderBy]: order });
      }
    }

    if (query.search) {
      args.where = {
        OR: [
          { name: { contains: query.search } },
          { email: { contains: query.search } },
          { firstName: { contains: query.search } },
          { lastName: { contains: query.search } },
        ],
      };
    }

    // console.log(args);
    // const validatedInclude = Prisma.validator<Prisma.UserInclude>()(args.include);

    const paginate = paginator({ perPage: paginatorPerPageDefault });

    return paginate<UserResponsePrismaPayload, Prisma.UserFindManyArgs>(
      this.prisma.user,
      args,
      {
        page: query.page,
        perPage: query.perPage,
      },
    ).catch((error) => {
      this.logger.log(error);
      throw new NotAcceptableException(error.message);
    });
  }

  async findById(id: string): Promise<UserResponsePrismaPayload> {
    return this.prisma.user
      .findUniqueOrThrow({
        where: { id },
        include: {
          lang: true,
        },
      })
      .catch((error) => {
        if (
          error instanceof Prisma.PrismaClientKnownRequestError &&
          error.code === 'P2025'
        ) {
          throw new NotFoundException();
        } else {
          this.logger.error(error.message, error);
          throw new InternalServerErrorException(error.message);
        }
      });
  }

  // Make sure no other user have similar login/email
  async similarUserExists(
    name: string | undefined,
    email: string | undefined,
    excludeId?: string,
    throwIfExists: boolean = true,
  ): Promise<boolean> {
    const or = [];

    if (name) {
      or.push({ name }, { email: name });
    }
    if (email) {
      or.push({ email }, { name: email });
    }

    const args: Prisma.UserFindFirstArgs & { where: Prisma.UserWhereInput } = {
      where: {
        OR: or,
      },
    };

    // When updating user the check should exclude self
    if (excludeId) {
      args.where.id = { not: excludeId };
    }

    const similarUser = !!(await this.prisma.user.findFirst(args));

    if (similarUser && throwIfExists) {
      throw new ConflictException('user_already_exist');
    }

    return !!similarUser;
  }

  async create(user: CreateUserDto): Promise<UserResponsePrismaPayload> {
    const data: Prisma.UserCreateInput = user;

    await this.similarUserExists(user.name, user.email);

    data.password = await this.passwordService.hashPassword(user.password);

    return this.prisma.user
      .create({
        data,
        include: {
          lang: true,
        },
      })
      .catch((error) => {
        this.logger.error(error.message, error);
        throw new NotAcceptableException(error.message);
      });
  }

  async update(
    id: string,
    user: UpdateUserDto,
  ): Promise<UserResponsePrismaPayload> {
    await this.similarUserExists(user.name, user.email, id);

    return this.prisma.user
      .update({
        where: { id },
        data: user,
        include: {
          lang: true,
        },
      })
      .catch((error) => {
        this.logger.error(error.message, error);
        throw new NotAcceptableException(error.message);
      });
  }

  async delete(id: string): Promise<UserResponsePrismaPayload> {
    return this.prisma.user.delete({
      where: { id },
      include: {
        lang: true,
      },
    });
  }

  // used by auth module to login user
  async findByLogin(login: string): Promise<User> {
    const user = await this.prisma.user.findFirst({
      where: {
        AND: [
          { NOT: { activatedAt: null } },
          { OR: [{ expiresAt: null }, { expiresAt: { gt: new Date() } }] },
          { OR: [{ email: login }, { name: login }] },
        ],
      },
    });

    if (!user) {
      throw new NotFoundException(
        `There isn't any active user with identifier: ${login}`,
      );
    }

    return user;
  }

  // async comparePassword(password: string, hash: string): Promise<boolean> {
  //   return bcrypt.compare(password, hash);
  // }

  // async hashPassword(password: string): Promise<string> {
  //   const saltRounds = 10;
  //   return bcrypt.hash(password, saltRounds);
  // }

  async updatePassword(
    reqUser: ReqUserDto,
    payload: UserUpdatePasswordDto,
  ): Promise<void> {
    const userId = reqUser.id;
    const authTokenId = reqUser.authToken.id;

    const user = await this.prisma.user.findUnique({
      select: { password: true },
      where: { id: userId },
    });

    // check current passwords
    const validateUser =
      user &&
      user.password &&
      (await this.passwordService.validatePassword(
        payload.password,
        user.password,
      ));

    if (!validateUser) {
      throw new BadRequestException('invalid password');
    }

    const hashedPassword = await this.passwordService.hashPassword(
      payload.newPassword,
    );

    const updateUserPassword = await this.prisma.user.update({
      where: { id: userId },
      data: { password: hashedPassword },
    });

    const deleteAllUserAuthTokensButCurrentlyUsed =
      await this.authTokenService.deleteAllUserTokens(userId, authTokenId);
  }
}
