import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsStrongPassword } from 'class-validator';
// import { isStrongPasswordOptions } from 'src/shared/constants';

export class UserUpdatePasswordDto {
  @IsNotEmpty()
  @ApiProperty({ description: `Current user password` })
  password: string;

  @IsNotEmpty()
  @ApiProperty()
  // @IsStrongPassword(isStrongPasswordOptions)
  newPassword: string;
}
