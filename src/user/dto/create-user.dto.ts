import { ApiProperty, PickType } from '@nestjs/swagger';
import { Prisma, User } from '@prisma/client';
import { Expose, Transform } from 'class-transformer';
import {
  IsNotEmpty,
  Length,
  IsEmail,
  IsStrongPassword,
  IsOptional,
  MaxLength,
  IsString,
} from 'class-validator';
// import { isStrongPasswordOptions } from 'src/shared/constants';

export class CreateUserDto
  // extends PickType(UserDto, [
  //   'name',
  //   'email',
  //   'password',
  //   'firstName',
  //   'lastName',
  //   'languageId',
  // ])
  implements Prisma.UserCreateInput
{
  @ApiProperty({
    type: String,
    required: true,
    minLength: 2,
    maxLength: 64,
  })
  @IsNotEmpty()
  @IsString()
  @Transform(({ value }) => value.trim())
  @Length(2, 64)
  name: string;

  @ApiProperty({
    type: String,
    required: true,
    example: 'example@example.com',
  })
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty({
    type: String,
    required: true,
  })
  @IsNotEmpty()
  // @IsStrongPassword(isStrongPasswordOptions)
  password: string;

  @ApiProperty({
    type: String,
    required: false,
    maxLength: 64,
    nullable: true,
  })
  @IsOptional()
  @MaxLength(64)
  firstName: string;

  @ApiProperty({
    type: String,
    required: false,
    maxLength: 64,
    nullable: true,
  })
  @IsOptional()
  @MaxLength(64)
  lastName: string;

  @ApiProperty({
    type: String,
    default: 'en',
    nullable: true,
  })
  @IsOptional()
  languageId: string;
}
