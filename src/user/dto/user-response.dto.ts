import { ApiProperty } from '@nestjs/swagger';
import { Prisma, User, UserRoleEnum } from '@prisma/client';
import { Exclude, Expose, Type } from 'class-transformer';
import { LangEntity } from 'src/lang/entities/lang.entity';
import { PaginatedResponseDto } from 'src/prisma/paginator/paginated-response.dto';

export type UserResponsePrismaPayloadArgs = {
  include: {
    lang: true;
  };
};

export type UserResponsePrismaPayload =
  Prisma.UserGetPayload<UserResponsePrismaPayloadArgs>;

export class UserResponseDto implements UserResponsePrismaPayload {
  @ApiProperty({
    type: String,
    format: 'uuid',
  })
  id: string;

  @ApiProperty({ nullable: true })
  provider: string | null;

  @ApiProperty({ nullable: true })
  providerId: string | null;

  @ApiProperty({
    type: String,
  })
  name: string;

  @ApiProperty({
    type: String,
    example: 'example@example.com',
  })
  email: string;

  @Exclude()
  password: string;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  firstName: string;

  @ApiProperty({
    type: String,
    nullable: true,
  })
  lastName: string;

  @ApiProperty({
    type: String,
    default: 'fr',
  })
  langId: string;

  @ApiProperty({
    type: LangEntity,
  })
  lang: LangEntity;

  @ApiProperty({ example: 'example@example.com', nullable: true })
  emailVerified: string;

  @Exclude()
  emailVerifyToken: string;

  @Exclude()
  forgotPasswordToken: string;

  @ApiProperty({ default: false })
  isSuperAdmin: boolean;

  @ApiProperty({ enum: UserRoleEnum })
  role: UserRoleEnum;

  @ApiProperty()
  activatedAt: Date;

  @ApiProperty()
  expiresAt: Date;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty({
    type: Boolean,
    default: false,
  })
  @Expose()
  @Type(() => Boolean)
  get isEmailVerified(): boolean {
    return this.email === this.emailVerified;
  }
}

export class UserResponsePaginatedDto extends PaginatedResponseDto<UserResponseDto> {
  // implements Awaited<ReturnType<AdminUserService['findManyPaginated']>>
  @ApiProperty({
    type: UserResponseDto,
    isArray: true,
  })
  @Type(() => UserResponseDto)
  data: UserResponseDto[];
}

// export class UserWithRoleResponseDto
//   extends UserResponseDto
//   implements UserWithRoleResponseDtoPrismaPayload
// {
//   @ApiProperty({
//     type: Boolean,
//     default: false,
//   })
//   @Expose()
//   @Type(() => Boolean)
//   get isEmailVerified(): boolean {
//     return this.email === this.emailVerified;
//   }

//   @ApiProperty({
//     type: RoleEntity,
//     isArray: true,
//   })
//   @Type(() => RoleEntity)
//   roles: RoleEntity[];

//   @ApiProperty({
//     type: LanguageEntity,
//     isArray: true,
//   })
//   @Type(() => LanguageEntity)
//   translatorLanguages: LanguageEntity[];

//   @ApiProperty({
//     type: LanguageEntity,
//   })
//   @Type(() => LanguageEntity)
//   language: LanguageEntity;
// }
