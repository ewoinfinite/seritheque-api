import { ApiPropertyOptional } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { PaginateOptionsDto } from 'src/prisma/paginator/paginate-options.dto';

export enum UserOrderBy {
  'name' = 'name',
  'email' = 'email',
  'createdAt' = 'createdAt',
  'updatedAt' = 'updatedAt',
  'langName' = 'langName',
}

export class UserFindManyQueryDto extends PaginateOptionsDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  search?: string;

  @ApiPropertyOptional({
    enum: UserOrderBy,
  })
  @IsOptional()
  @IsEnum(UserOrderBy)
  orderBy?: UserOrderBy;

  @ApiPropertyOptional({
    enum: Prisma.SortOrder,
    default: Prisma.SortOrder.asc,
  })
  @IsOptional()
  @IsEnum(Prisma.SortOrder)
  order?: Prisma.SortOrder;
}
