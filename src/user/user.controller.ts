import {
  Controller,
  UseGuards,
  Request,
  Post,
  Body,
  HttpStatus,
  Patch,
  Req,
} from '@nestjs/common';
import { UserService } from './user.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserUpdatePasswordDto } from './dto/user-update-password.dto';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { ReqUserDto } from 'src/auth/dto/req-user.dto';
import { UpdateUserDto } from './dto/update-user-dto';
import { plainToInstance } from 'class-transformer';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { UserResponseDto } from './dto/user-response.dto';

@Controller('user')
@UseGuards(JwtAuthGuard, RolesGuard)
@Roles('ADMIN')
@ApiBearerAuth()
@ApiTags('User')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('update-password')
  @ApiResponse({
    type: Object,
    status: HttpStatus.CREATED,
    description: `User password update`,
  })
  async updatePassword(
    @Req() req: Express.Request & { user: ReqUserDto },
    @Body() body: UserUpdatePasswordDto,
  ) {
    const reqUser: ReqUserDto = req.user;

    await this.userService.updatePassword(reqUser, body);

    return {
      success: true,
      message: `User password update`,
    };
  }

  @Patch('')
  @ApiOkResponse({ type: UserResponseDto })
  async update(
    @Req() req: Express.Request & { user: ReqUserDto },
    @Body() body: UpdateUserDto,
  ): Promise<UserResponseDto> {
    return plainToInstance(
      UserResponseDto,
      this.userService.update(req.user.id, body),
    );
  }
}
