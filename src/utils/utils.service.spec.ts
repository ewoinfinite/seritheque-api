import { Test, TestingModule } from '@nestjs/testing';
import { UtilsService } from './utils.service';

describe('UtilsService', () => {
  let service: UtilsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UtilsService],
    }).compile();

    service = module.get<UtilsService>(UtilsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('txt2md', () => {
    //     it('should rebuild indentation', () => {
    //       const txt = `• Indicateur : volume horaire de suc, ml
    // • Stimulus :
    //     ° jus, bouillon de chou : 50-110
    //     ° histamine : 100-150`;
    //       const md = service.txt2md(txt);

    //       expect(md).toBe(`- Indicateur : volume horaire de suc, ml
    // - Stimulus :
    //     - jus, bouillon de chou : 50-110
    //     - histamine : 100-150`);
    //     });

    it('should use dash as list element and preserve indentation', () => {
      const txt = `First paragraph.

• Indicateur : volume horaire de suc, ml
• Stimulus :
    ° jus, bouillon de chou : 50-110
    ° histamine : 100-150`;

      const md = service.txt2md(txt);

      expect(md).toBe(`First paragraph.

- Indicateur : volume horaire de suc, ml
- Stimulus :
    - jus, bouillon de chou : 50-110
    - histamine : 100-150`);
    });
  });

  describe('txtLocal', () => {
    it(`should use a thin nbsp in front of punctuation in french`, () => {
      const txt = `- Indicateur : volume horaire de suc, ml`;

      const txtFr = service.txtLocal(txt, 'fr');

      expect(txtFr).toBe('- Indicateur : volume horaire de suc, ml');
    });

    it(`should not touch to time in french`, () => {
      const txt = `Rendez-vous à 14:00`;

      const txtFr = service.txtLocal(txt, 'fr');

      expect(txtFr).toBe('Rendez-vous à 14:00');
    });

    it('should use a thin nbsp in front of punctuation in french after twisted cases', () => {
      const txt = `- Teneur en mmol/l [Keys et al., 1950] : 3,4-6,4`;
      const convertedTxt = service.txtLocal(txt, 'fr');

      expect(convertedTxt).toBe(
        `- Teneur en mmol/l [Keys et al., 1950] : 3,4-6,4`,
      );
    });
  });

  describe('convertTabIndentationToSpaces', () => {
    it('should convert single tab to 4 spaces', () => {
      const txt = `
\tThis is a test
`;
      const convertedTxt = service.convertTabIndentationToSpaces(txt, 4);

      expect(convertedTxt).toBe(`
    This is a test
`);
    });

    it('should convert 2 tabs to 8 spaces', () => {
      const txt = `
\t- This is a test
\t\t- This is a test2
`;
      const convertedTxt = service.convertTabIndentationToSpaces(txt, 4);

      expect(convertedTxt).toBe(`
    - This is a test
        - This is a test2
`);
    });

    it('should not touch tabs in text', () => {
      const txt = `
\t- This is a test
\t\t- This is a\ttest2
`;
      const convertedTxt = service.convertTabIndentationToSpaces(txt, 4);

      expect(convertedTxt).toBe(`
    - This is a test
        - This is a\ttest2
`);
    });
  });

  describe('convertSpacesToSpacesIndentation', () => {
    it('should leave tabs untouched', () => {
      const txt = `
\tThis is a test
`;
      const convertedTxt = service.convertSpacesToSpacesIndentation(txt, 2, 4);

      expect(convertedTxt).toBe(`
\tThis is a test
`);
    });

    it('should replace 2 space with 4', () => {
      const txt = `
  This is a test
`;
      const convertedTxt = service.convertSpacesToSpacesIndentation(txt, 2, 4);

      expect(convertedTxt).toBe(`
    This is a test
`);
    });

    it('should replace 4 space with 8', () => {
      const txt = `
    This is a test
`;
      const convertedTxt = service.convertSpacesToSpacesIndentation(txt, 2, 4);

      expect(convertedTxt).toBe(`
        This is a test
`);
    });

    it('should replace 5 space with 9', () => {
      const txt = `
     This is a test
`;
      const convertedTxt = service.convertSpacesToSpacesIndentation(txt, 2, 4);

      expect(convertedTxt).toBe(`
         This is a test
`);
    });

    it('should convert 4 spaces indentation to 2 (5 spaces to 3)', () => {
      const txt = `
     This is a test
`;
      const convertedTxt = service.convertSpacesToSpacesIndentation(txt, 4, 2);

      expect(convertedTxt).toBe(`
   This is a test
`);
    });
  });

  describe('fixTxt', () => {
    it('should add a space to columns in french', () => {
      const txt = `ex: ceci est un test`;
      const convertedTxt = service.fixTxt(txt, 'fr');

      expect(convertedTxt).toBe(`ex : ceci est un test`);
    });

    it('should add a space to columns in french', () => {
      const txt = `- Teneur en mmol/l [Keys et al., 1950]: 3,4-6,4`;
      const convertedTxt = service.fixTxt(txt, 'fr');

      expect(convertedTxt).toBe(
        `- Teneur en mmol/l [Keys et al., 1950] : 3,4-6,4`,
      );
    });

    it('should add a space to question mark in french', () => {
      const txt = `Ceci est un test?`;
      const convertedTxt = service.fixTxt(txt, 'fr');

      expect(convertedTxt).toBe(`Ceci est un test ?`);
    });

    it('should add a space to question mark in french and right trim', () => {
      const txt = `Ceci est un test? `;
      const convertedTxt = service.fixTxt(txt, 'fr');

      expect(convertedTxt).toBe(`Ceci est un test ?`);
    });

    it('should not alter a time in french', () => {
      const txt = `Il est 14:00 du matin.`;
      const convertedTxt = service.fixTxt(txt, 'fr');

      expect(convertedTxt).toBe(`Il est 14:00 du matin.`);
    });

    it('should remplace multiple spaces with single', () => {
      const txt = `Il est 14:00 du   matin.`;
      const convertedTxt = service.fixTxt(txt, 'fr');
      expect(convertedTxt).toBe(`Il est 14:00 du matin.`);
    });

    it('should remplace tabs with single space', () => {
      const txt = `Il est 14:00 du\tmatin.`;
      const convertedTxt = service.fixTxt(txt, 'fr');
      expect(convertedTxt).toBe(`Il est 14:00 du matin.`);
    });

    it('should not touch indentation tabs and remplace inner tabs with single space', () => {
      const txt = `\t- Il est 14:00 du\tmatin.`;
      const convertedTxt = service.fixTxt(txt, 'fr');
      expect(convertedTxt).toBe(`\t- Il est 14:00 du matin.`);
    });

    it('should remove leading or trailing empty lines preserving indentation', () => {
      const txt = `\n    - Il est 14:00 du matin.\n`;
      const convertedTxt = service.fixTxt(txt, 'fr');
      expect(convertedTxt).toBe(`    - Il est 14:00 du matin.`);
    });
  });
});
