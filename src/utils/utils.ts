import { convert } from 'html-to-text';
import { MarkedOptions, marked } from 'marked';
import { JSDOM } from 'jsdom';
import * as DOMPurify from 'dompurify';

export const txt2md = (txt: string): string => {
  return txt
    .split(/\n/)
    .map((t) => t.replace(/^(\s*)[-•°*]\s+/, '$1- '))
    .join('\n');
};

export const unAccent = (txt: string): string => {
  // credit https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript
  return txt.normalize('NFKD').replace(/\p{Diacritic}/gu, '');
};

export const txtLocal = (txt: string, lang?: string): string => {
  switch (lang) {
    // @ts-expect-error: Case with fallthrough in instruction switch. ts(7029)
    case 'fr':
      txt = txt.replace(/(\S) ([?!:;])(\s|$)/gmu, '$1\u202F$2$3'); // \u202F = Espace insécable étroite (NNBSP)

    case 'default':
    default:
  }

  return txt;
};

export const fixTxtFr = (txt: string): string =>
  txt.replace(/(?<=\S)\s*([?!:;])(\s+|$)/gmu, ' $1$2'); // punctuation takes 1 space before

export const fixTxt = (txt: string, lang?: string) => {
  txt = txt.replace(/\s+$/gm, ''); // right trim

  txt = txt
    .split(/\n/)
    .map((t) => {
      // protect markdown tables
      const t2 = t.replace('|', '');
      if (t.length - t2.length < 2) {
        // we have no more than 1 |
        t = t.replace(/(?<!^\s*)\s+/g, ' '); // any space not indented
      }

      return t;
    })
    .join('\n');

  txt = txt.replace(/(?:^\n+|\n+$)/g, '').replace(/\n{2,}/g, '\n\n'); // leading/trailing empty lines or more than two empty lines

  switch (lang) {
    case 'fr':
      txt = fixTxtFr(txt);
      break;
  }

  return txt;
};

export const convertSpacesToSpacesIndentation = (
  txt: string,
  nbSpaces: number = 2,
  toNbSpaces: number = 4,
): string => {
  const regex = new RegExp(`^([^\\S\\t]{${nbSpaces}})+`, '');

  return txt
    .split(/\n/)
    .map((l) => {
      const execRegex = regex.exec(l);

      return execRegex
        ? l.replace(
            regex,
            ' '.repeat(toNbSpaces * Math.floor(execRegex[0].length / nbSpaces)),
          )
        : l;
    })
    .join('\n');
};

export const convertTabIndentationToSpaces = (
  txt: string,
  nbSpaces: number = 4,
): string => {
  const regex = new RegExp(/^\t+/);

  return txt
    .split(/\n/)
    .map((l) => {
      const execRegex = regex.exec(l);

      return execRegex
        ? l.replace(regex, ' '.repeat(nbSpaces * execRegex[0].length))
        : l;
    })
    .join('\n');
};

export function fixHtml(html: string): string {
  const window = new JSDOM('').window;
  const purify = DOMPurify(window);
  return purify.sanitize(html);
}

export function md2html(
  text: string,
  options?: Omit<MarkedOptions, 'async'>,
): string {
  // linter seems tu be confused: string | Promise<string>

  if (!text) return '';

  const html = <string>marked.parse(text, {
    silent: false,
    gfm: true,
    breaks: true,
    ...options,
    async: false,
  });

  return fixHtml(html);
}

export function md2txt(md: string): string {
  return convert(md2html(md));
}
