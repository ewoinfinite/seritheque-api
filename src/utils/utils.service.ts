import { Injectable } from '@nestjs/common';
import {
  convertSpacesToSpacesIndentation,
  convertTabIndentationToSpaces,
  fixTxt,
  txt2md,
  txtLocal,
  unAccent,
} from './utils';
import { SimpleCategoryDto } from 'src/category/dto/simple-category.dto';
import { CategoryWithContentsDto } from 'src/category/dto/category-with-contents.dto';
import { SerDetailDto } from 'src/ser/dto/ser-detail.dto';
import { SimpleSerDetailDto } from 'src/ser/dto/simple-ser-detail.dto';
import { CategoryWithCountSers } from 'src/category/dto/category-with-count-sers.dto';

@Injectable()
export class UtilsService {
  txt2md = txt2md;

  unAccent = unAccent;

  txtLocal = txtLocal;

  convertTabIndentationToSpaces = convertTabIndentationToSpaces;

  convertSpacesToSpacesIndentation = convertSpacesToSpacesIndentation;

  fixTxt = fixTxt;

  categoryWithContentsToCategorySimple(
    category: CategoryWithContentsDto,
  ): SimpleCategoryDto {
    return {
      id: category.id,
      parentId: category.parentId,
      order: category.order,
      name: category.contents[0]?.name,
      // count: category._count.sers,
    };
  }

  readonly serDetailToSimpleSerDetail = (
    ser: SerDetailDto,
  ): SimpleSerDetailDto => {
    return {
      ...ser,
      simpleCategories: ser.categories.map((cat) =>
        this.categoryWithContentsToCategorySimple(cat),
      ),
    };
  };
}
