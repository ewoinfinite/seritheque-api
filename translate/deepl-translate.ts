import axios from 'axios';
import _ from 'lodash';

export type DeeplTranslateParams = {
  target_lang: string;
  text: string[];
  source_lang?: string;
  tag_handling?: string; // ex: html
  context?: string;
  model_type?:
    | 'latency_optimized'
    | 'quality_optimized'
    | 'prefer_quality_optimized';
};

export type DeeplTranslateResponse = {
  translations: {
    detected_source_language: string; // ex 'EN'
    text: string;
  }[];
};

export const deeplTranslate = async (
  params: DeeplTranslateParams,
): Promise<DeeplTranslateResponse> => {
  const DEEPL_API_URL = process.env.DEEPL_API_URL;
  const DEEPL_AUTH_KEY = process.env.DEEPL_AUTH_KEY;

  // remove keys with value of undefined
  // Object.keys(params).forEach(
  //   (key) => params[key] === undefined && delete params[key],
  // );

  let key: keyof typeof params;
  for (key in params) {
    params[key] === undefined && delete params[key];
  }

  params.source_lang = params.source_lang?.toUpperCase();
  params.target_lang = params.target_lang?.toUpperCase();

  params.model_type = params.model_type ?? 'prefer_quality_optimized';

  // _.pickBy(params);

  try {
    // console.log(
    //   `fetching ${params.text.length} string(s) from DeepL api (${humanFileSize(nbBytes)} ${params.source_lang || '?'}=>${params.target_lang} ${params.tag_handling || 'text'})`,
    // );

    const nbBytes: number = params.text.reduce(
      (accumulator, text) =>
        accumulator + new TextEncoder().encode(text).length,
      new TextEncoder().encode(params.context ?? '').length,
    );

    console.log(
      `fetching ${params.text.length} string(s) from DeepL api (${humanFileSize(nbBytes)} ${params.source_lang || '?'}=>${params.target_lang} ${params.tag_handling || 'text'})`,
    );

    const response = await axios({
      method: 'post',
      url: DEEPL_API_URL,
      headers: {
        Authorization: `DeepL-Auth-Key ${DEEPL_AUTH_KEY}`,
        'Content-Type': 'application/json',
      },
      data: params,
    });

    console.log('Done.');

    return response.data;
  } catch (error) {
    console.error(error.response?.data?.message || error);

    throw error;

    // throw new ServiceUnavailableException(
    //   error.response?.data?.message || error.message,
    // );
  }
};

export const humanFileSize = (b: number): string => {
  var u = 0,
    s = 1024;
  while (b >= s || -b >= s) {
    b /= s;
    u++;
  }
  return (u ? b.toFixed(2) : b) + (u ? ' KMGTPEZY'[u] : '') + 'B';
};
