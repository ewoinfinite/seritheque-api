import { Prisma, PrismaClient } from '@prisma/client';
import { deeplTranslate } from './deepl-translate';

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  // log: ['warn', 'error'],
});

export async function translateCategoriesBatch(
  toLang: string,
  fromLang: string = 'fr',
  take: number = 50,
): Promise<number> {
  const categories = await prisma.category.findMany({
    where: {
      contents: {
        none: { langId: toLang },
        some: { langId: fromLang },
      },
    },
    include: {
      contents: { where: { langId: fromLang } },
      // _count: {},
    },

    take,
    // { contents: { some langId: toLang } }
  });

  let nb = categories.length;

  if (!nb) return nb;

  console.log(`Found ${categories.length}`);

  console.log(categories);

  const translations = await deeplTranslate({
    target_lang: toLang,
    source_lang: fromLang,
    text: categories.map((cat) => cat.contents[0].name),
  }).then((r) => r.translations);

  for (let i in translations) {
    const data: Prisma.CategoryContentCreateArgs['data'] = {
      categoryId: categories[i].id,
      langId: toLang,
      name: translations[i].text,
    };

    // console.log(data);
    await prisma.categoryContent
      .create({
        data,
      })
      .catch((err) => {
        console.log(data);
        throw err;
      });
  }

  return nb;
}

export async function translateCategories(
  toLang: string,
  fromLang: string = 'fr',
  take?: number,
): Promise<number> {
  let total = 0;

  while (true) {
    let nb = await translateCategoriesBatch(toLang, fromLang, take);

    if (!nb) break;

    total += nb;
  }

  return total;
}

translateCategories('en', 'fr').then((total) =>
  console.log(`Completed ${total} translations en`),
);

translateCategories('es', 'fr').then((total) =>
  console.log(`Completed ${total} translations es`),
);

translateCategories('it', 'fr').then((total) =>
  console.log(`Completed ${total} translations it`),
);

translateCategories('pt', 'fr').then((total) =>
  console.log(`Completed ${total} translations pt`),
);

translateCategories('de', 'fr').then((total) =>
  console.log(`Completed ${total} translations de`),
);
