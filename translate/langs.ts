import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  log: ['warn', 'error'],
});

const createLang = async (id: string, name: string, localName: string) => {
  const lang = await prisma.lang.findUnique({ where: { id } });

  if (lang) return;

  await prisma.lang.create({
    data: { id, name, localName },
  });

  console.log(`Created lang ${id} - ${name} - ${localName}`);
};

createLang('fr', 'French', 'Français');
createLang('en', 'English', 'English');
createLang('es', 'Spanish', 'Castellano');
createLang('it', 'Italian', 'Italiano');
createLang('pt', 'Portuguese', 'Português');
createLang('de', 'German', 'Deutsch');
