import { PrismaClient } from '@prisma/client';
import { deeplTranslate } from './deepl-translate';
import { md2txt, unAccent } from '../src/utils/utils';
import { excerptNbWords } from '../src/shared/constants';

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  // log: ['warn', 'error'],
});

export async function translateSersBatch(
  toLang: string,
  fromLang: string = 'fr',
  take: number = 50,
): Promise<number> {
  const sers = await prisma.ser.findMany({
    where: {
      contents: {
        none: { langId: toLang },
        some: { langId: fromLang },
      },
    },
    include: {
      contents: { where: { langId: fromLang } },
      // _count: {},
    },

    take,
    // { contents: { some langId: toLang } }
  });

  let nb = sers.length;

  if (!nb) return nb;

  console.log(`Found ${sers.length} sers`);

  // console.log(sources);

  const translations = await deeplTranslate({
    target_lang: toLang,
    source_lang: fromLang,
    text: sers.map(
      (ser) => `${ser.contents[0].title}\n----\n${ser.contents[0].content}`,
    ),
  }).then((r) => r.translations);

  for (let i in translations) {
    const splits = translations[i].text.split('\n----\n');
    const title = splits[0];
    const noAccentTitle = unAccent(title).toLowerCase();

    const content: string | null = splits[1] ?? null;

    const excerptA = md2txt(content).split(/\s/);

    const excerpt =
      excerptA.slice(0, excerptNbWords).join(' ') +
      (excerptA.length > excerptNbWords ? '…' : '');

    // console.log(data);
    await prisma.serContent
      .create({
        data: {
          serId: sers[i].id,
          langId: toLang,
          title,
          content,
          excerpt,
          noAccentTitle,
        },
      })
      .catch((err) => {
        // console.log(data);
        throw err;
      });
  }

  return nb;
}

export async function translateSers(
  toLang: string,
  fromLang: string = 'fr',
  take?: number,
): Promise<number> {
  let total = 0;

  while (true) {
    let nb = await translateSersBatch(toLang, fromLang, take);

    if (!nb) break;

    total += nb;
  }

  return total;
}

translateSers('en', 'fr').then((total) =>
  console.log(`Completed ${total} translations en`),
);

translateSers('es', 'fr').then((total) =>
  console.log(`Completed ${total} translations es`),
);

translateSers('it', 'fr').then((total) =>
  console.log(`Completed ${total} translations it`),
);

translateSers('pt', 'fr').then((total) =>
  console.log(`Completed ${total} translations pt`),
);

translateSers('de', 'fr').then((total) =>
  console.log(`Completed ${total} translations de`),
);
