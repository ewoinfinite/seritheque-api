import { Prisma, PrismaClient } from '@prisma/client';
import { deeplTranslate } from './deepl-translate';

const prisma = new PrismaClient({
  // log: ['query', 'info', 'warn', 'error'],
  // log: ['warn', 'error'],
});

export async function translateSourcesBatch(
  toLang: string,
  fromLang: string = 'fr',
  take: number = 50,
): Promise<number> {
  const sources = await prisma.source.findMany({
    where: {
      contents: {
        none: { langId: toLang },
        some: { langId: fromLang },
      },
    },
    include: {
      contents: { where: { langId: fromLang } },
      // _count: {},
    },

    take,
    // { contents: { some langId: toLang } }
  });

  let nb = sources.length;

  if (!nb) return nb;

  console.log(`Found ${sources.length}`);

  console.log(sources);

  const translations = await deeplTranslate({
    target_lang: toLang,
    source_lang: fromLang,
    text: sources.map((source) => source.contents[0].title),
  }).then((r) => r.translations);

  for (let i in translations) {
    const data: Prisma.SourceContentCreateArgs['data'] = {
      sourceId: sources[i].id,
      langId: toLang,
      title: translations[i].text,
    };

    // console.log(data);
    await prisma.sourceContent
      .create({
        data,
      })
      .catch((err) => {
        console.log(data);
        throw err;
      });
  }

  return nb;
}

export async function translateSources(
  toLang: string,
  fromLang: string = 'fr',
  take?: number,
): Promise<number> {
  let total = 0;

  while (true) {
    let nb = await translateSourcesBatch(toLang, fromLang, take);

    if (!nb) break;

    total += nb;
  }

  return total;
}

translateSources('en', 'fr').then((total) =>
  console.log(`Completed ${total} translations en`),
);

translateSources('es', 'fr').then((total) =>
  console.log(`Completed ${total} translations es`),
);

translateSources('it', 'fr').then((total) =>
  console.log(`Completed ${total} translations it`),
);

translateSources('pt', 'fr').then((total) =>
  console.log(`Completed ${total} translations pt`),
);

translateSources('de', 'fr').then((total) =>
  console.log(`Completed ${total} translations de`),
);
