#!/bin/bash


sqlitedb=tmp/imp.sqlite
csv=tmp/imp.csv

source $(dirname "$0")/.env

# echo "DROP DATABASE $db WITH (force); CREATE DATABASE $db;" | psql

# read -p "Are you sure you want do DROP DATABASE '${db}'? (yN)" -n 1 -r
read -p "Are you sure you want do DROP DATABASE '${db}'? (yN)"
# echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo "DROP DATABASE $db WITH (FORCE);" | psql postgresql://${dbuser}:${dbpass}@localhost:5432/${db}
  echo "CREATE DATABASE $db;" | psql postgresql://${dbuser}:${dbpass}@localhost:5432/${db}
fi

[[ -f "${csv}" ]] || {
  echo "Could not find csv file ${csv}";
  exit 1;
}

[[ -f "${sqlitedb}" ]] && {
  rm -i "${sqlitedb}";
}

# echo "Importing ${csv} to ${sqlitedb}..."
# echo ".import \"${csv}\" --csv imp" | sqlite3 "${sqlitedb}"
# echo "Done."

echo "Importing ${csv} to ${sqlitedb}..."
sqlite3 << EOF
.mode csv
.import $csv imp
.save $sqlitedb
EOF
echo "Done."

echo "Create postgresql DB and migrate the schema"
npx prisma migrate dev
echo "Done."

echo "Create imp table in postgresql DB"
pgloader "sqlite://${sqlitedb}" "pgsql://${dbuser}:${dbpass}@localhost:5432/${db}"
echo "Done."

echo "Processing import..."
npm run prisma:seed
npm run prisma:import
echo "Done."

